/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : tour

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-11-11 09:36:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_article
-- ----------------------------
DROP TABLE IF EXISTS `tbl_article`;
CREATE TABLE `tbl_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `intro_text` text CHARACTER SET utf8,
  `content` longtext CHARACTER SET utf8,
  `image` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT '0',
  `publish_up` timestamp NULL DEFAULT NULL,
  `publish_down` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `featured` tinyint(4) DEFAULT '0',
  `hit` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_article
-- ----------------------------
INSERT INTO `tbl_article` VALUES ('1', '1', 'Hello world', null, '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', '1', '1541464993', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('2', '2', 'Hello Daytour', null, '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('3', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('4', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('5', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('6', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('7', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('8', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('9', '1', 'Hello world', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', '1', '1541464993', null, null, '1', '0', '0');
INSERT INTO `tbl_article` VALUES ('10', '2', 'Hello Daytour', '', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, '1', '0', '0');

-- ----------------------------
-- Table structure for tbl_category
-- ----------------------------
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` char(30) DEFAULT '0',
  `type` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `featured` tinyint(4) DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_category
-- ----------------------------
INSERT INTO `tbl_category` VALUES ('1', '007-001-', 'tour', 'Danh muc 1', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('2', '002-', 'tour', 'Danh muc 2', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('3', '003-', 'tour', 'Danh muc 3', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('4', '007-001-004-', 'tour', 'Danh muc 1.3', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('5', '007-001-005-', 'tour', 'Danh muc 1.1', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('6', '007-001-006-', 'tour', 'Danh muc 1.2', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('7', '007-', 'tour', 'Danh muc 4', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('11', '007-014-', 'tour', 'Danh mục 4.2', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('13', '007-013-', 'tour', 'Danh mục 4.1', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('15', '015-', 'tour', 'Danh mục 4.2', null, null, '0', null, '1');
INSERT INTO `tbl_category` VALUES ('16', '016-', 'tour', 'Cẩm nang du lịch', null, null, '0', null, '1');

-- ----------------------------
-- Table structure for tbl_customer
-- ----------------------------
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` text NOT NULL,
  `active` int(11) NOT NULL,
  `city_code` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO `tbl_customer` VALUES ('1', 'Ch? Ng?c Ly', 'ngoclychu@gmail.com', '916131991', 'Nguy?n Trãi, Thanh Xuân', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('2', 'Ch? Ng?c Ly', 'ngoclychu@gmail.com', '916131991', 'Nguy?n Trãi, Thanh Xuân', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('3', 'Chu Ngoc Ly', 'ngocly.chu@mail.com', '6317291868', '14033  SE STARK ST PORTLAND, OR 97233 UNITED STATES', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('4', 'Chu Ngoc Ly', 'ngocly.chu@mail.com', '6317291868', '14033  SE STARK ST PORTLAND, OR 97233 UNITED STATES', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('5', 'Chu Ngoc Ly', 'ngocly.chu@mail.com', '6317291868', '14033  SE STARK ST PORTLAND, OR 97233 UNITED STATES', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('6', 'Thanh Phan', 'thanhpv@dichung.vn', '977247006', 'Hue Street', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('7', '', '', '', '', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('8', 'Ch? Ng?c Ly', 'ngoclychu@gmail.com', '916131991', 'Nguy?n Trãi, Thanh Xuân', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('9', 'Ch? Ng?c Ly', 'ngoclychu@gmail.com', '916131991', 'Nguy?n Trãi, Thanh Xuân', '0', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_customer` VALUES ('10', 'Thanh Phan', 'thanhpv@dichung.vn', '977247006', 'Hue Street', '0', '0000-00-00 00:00:00', '', '0', '0');

-- ----------------------------
-- Table structure for tbl_destination
-- ----------------------------
DROP TABLE IF EXISTS `tbl_destination`;
CREATE TABLE `tbl_destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `featured` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_destination
-- ----------------------------
INSERT INTO `tbl_destination` VALUES ('1', 'Clark', 'clark', null, 'http://localhost/touristment/uploads/images/shutterstock_255194035-800x960.jpg', null, '1', '1');
INSERT INTO `tbl_destination` VALUES ('2', 'Hà Nội', 'ha-noi', null, 'http://localhost/touristment/uploads/images/shutterstock_147744218-800x960.jpg', null, '1', '1');
INSERT INTO `tbl_destination` VALUES ('3', 'Hải Phòng', 'hai-phong', null, 'http://localhost/touristment/uploads/images/shutterstock_124333858-800x960.jpg', null, '1', '0');
INSERT INTO `tbl_destination` VALUES ('4', 'Cà Mau', 'ca-mau', null, 'http://localhost/touristment/uploads/images/shutterstock_120562819-800x960.jpg', null, '1', '1');
INSERT INTO `tbl_destination` VALUES ('5', 'Phú Quốc', 'phu-quoc', null, 'http://localhost/touristment/uploads/images/Fotolia_16069076_Subscription_Monthly_XXL-800x960.jpg', null, '1', '1');
INSERT INTO `tbl_destination` VALUES ('6', 'Đà Nẵng', null, null, '', null, '0', '1');
INSERT INTO `tbl_destination` VALUES ('7', 'Cô Tô', null, null, '', null, '0', '1');
INSERT INTO `tbl_destination` VALUES ('8', 'Cát Bà', null, null, '', null, '0', '1');

-- ----------------------------
-- Table structure for tbl_options
-- ----------------------------
DROP TABLE IF EXISTS `tbl_options`;
CREATE TABLE `tbl_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `value` longtext CHARACTER SET utf8,
  `autoload` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_options
-- ----------------------------
INSERT INTO `tbl_options` VALUES ('1', 'main-menu', '[[{\"link\":\"/touristment/tour/category?id=16\",\"title\":\"Cẩm nang du lịch\",\"children\":[[]]}]]', '1');
INSERT INTO `tbl_options` VALUES ('2', 'slider', '{\"show_nav\":\"0\",\"show_dots\":\"0\",\"auto_play\":\"1\",\"timeout\":\"1\",\"source\":[\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/slider\\/185310.jpg\",\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/slider\\/moscow_russia_red_square_light_evening_69285_1920x1080.jpg\",\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/slider\\/city_moscow_night_lights_bridge_reflection_river_59092_1920x1080.jpg\"]}', '1');
INSERT INTO `tbl_options` VALUES ('3', 'logo', '{\"source\":\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/logo.png\"}', '1');

-- ----------------------------
-- Table structure for tbl_page
-- ----------------------------
DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `intro_text` text CHARACTER SET utf8,
  `content` longtext CHARACTER SET utf8,
  `image` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT '0',
  `publish_up` timestamp NULL DEFAULT NULL,
  `publish_down` timestamp NULL DEFAULT NULL,
  `status` char(10) DEFAULT 'publish' COMMENT 'publish, unpublish, trash',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_page
-- ----------------------------
INSERT INTO `tbl_page` VALUES ('1', '1', 'Hello world', 'hello-world', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, 'publish');
INSERT INTO `tbl_page` VALUES ('2', '2', 'Hello Daytour', 'hello-daytour', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&u', '<p>Đời sống t&iacute;n ngưỡng văn h&oacute;a t&acirc;m linh của người Việt&nbsp;Nam&nbsp;kh&ocirc;ng thể thiếu những đền ch&ugrave;a miếu mạo. Nhiều ng&ocirc;i ch&ugrave;a đ&atilde; được x&acirc;y dựng từ rất l&acirc;u đời, l&agrave; di t&iacute;ch lịch sử quan trọng của quốc gia. Trong đ&oacute; phải kể đến&nbsp;Ch&ugrave;a Một Cột, đ&oacute;a sen&nbsp;trầm mặc giữa l&ograve;ng Thủ Đ&ocirc; H&agrave; Nội.</p>\r\n<p><strong>Ch&ugrave;a Một Cột</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;Đ&agrave;i Li&ecirc;n Hoa&nbsp;tượng trưng cho vẻ đẹp ngh&igrave;n năm văn hiến của H&agrave; Nội. Theo tương truyền, ch&ugrave;a được x&acirc;y dựng v&agrave;o năm 1409, khi vua L&yacute; Th&aacute;i T&ocirc;ng mơ thấy Phật B&agrave; Quan &Acirc;m ngự tr&ecirc;n đ&oacute;a sen dắt người l&ecirc;n t&ograve;a. Khi nh&agrave; vua kể lại cho bề t&ocirc;i, đ&atilde; nghe theo lời khuy&ecirc;n dựng ch&ugrave;a của Nh&agrave; sư Thiền Tu&ecirc;. Ch&ugrave;a Một Cột được thiết kế dựa tr&ecirc;n giấc mơ vua L&yacute; Th&aacute;i T&ocirc;ng , bao gồm một gian nằm tr&ecirc;n cột đ&aacute; tựa h&igrave;nh d&aacute;ng b&ocirc;ng sen (Đ&agrave;i Li&ecirc;n Hoa), b&ecirc;n tr&ecirc;n c&oacute; tượng Phật B&agrave; Quan &Acirc;m.</p>', 'http://localhost/touristment/uploads/images/lake_tahoe.jpg', null, '1540030999', null, null, 'publish');

-- ----------------------------
-- Table structure for tbl_tag
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tag`;
CREATE TABLE `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_tag
-- ----------------------------
INSERT INTO `tbl_tag` VALUES ('1', 'Hải Phòng', null);
INSERT INTO `tbl_tag` VALUES ('2', 'hà nội', null);
INSERT INTO `tbl_tag` VALUES ('3', 'Cà mau', null);

-- ----------------------------
-- Table structure for tbl_tag_object
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tag_object`;
CREATE TABLE `tbl_tag_object` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_table` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tag` (`tag_id`),
  KEY `object` (`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_tag_object
-- ----------------------------
INSERT INTO `tbl_tag_object` VALUES ('1', '1', '1', 'tbl_article');
INSERT INTO `tbl_tag_object` VALUES ('2', '2', '1', 'tbl_article');

-- ----------------------------
-- Table structure for tbl_tour
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour`;
CREATE TABLE `tbl_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `intro_text` text CHARACTER SET utf8,
  `departure_location` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `destination` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `duration_days` int(11) DEFAULT '0',
  `duration_nights` int(11) DEFAULT '0',
  `price` varchar(100) DEFAULT '0',
  `images` text,
  `featured` tinyint(4) DEFAULT '0',
  `sale` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL,
  `publish_up` date DEFAULT NULL,
  `publish_down` date DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tour_category` (`cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour
-- ----------------------------
INSERT INTO `tbl_tour` VALUES ('1', '2', 'Du lịch Tahoe', 'du-lich-tahoe', '<p>Du lịch th&aacute;i lan</p>', 'hanoi', '2,3,6', '1', '2', '{\"base\":\"10000\",\"sale\":\"9699\",\"kid\":\"9000\",\"baby\":\"970\"}', '{\"full\":\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/background_tour_detail_demo.png\",\"intro\":\"http:\\/\\/localhost\\/touristment\\/uploads\\/images\\/lake_tahoe.jpg\"}', '0', '1', '1', null, null, '1539786874', null);

-- ----------------------------
-- Table structure for tbl_tour_departures
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_departures`;
CREATE TABLE `tbl_tour_departures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `slots` tinyint(11) DEFAULT '0',
  `booked_slots` tinyint(4) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT '0',
  `sale_price` decimal(10,0) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tour` (`tour_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour_departures
-- ----------------------------
INSERT INTO `tbl_tour_departures` VALUES ('1', '1', '2018-10-24', '0', '4', '10000', '0');
INSERT INTO `tbl_tour_departures` VALUES ('2', '1', '2018-10-25', '0', '2', '10000', '0');
INSERT INTO `tbl_tour_departures` VALUES ('3', '1', '2018-10-26', '0', '0', '10000', '0');
INSERT INTO `tbl_tour_departures` VALUES ('4', '1', '2018-10-27', '0', '0', '10000', '0');

-- ----------------------------
-- Table structure for tbl_tour_destination
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_destination`;
CREATE TABLE `tbl_tour_destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tour` (`tour_id`) USING BTREE,
  KEY `destination` (`destination_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour_destination
-- ----------------------------
INSERT INTO `tbl_tour_destination` VALUES ('1', '1', '2');
INSERT INTO `tbl_tour_destination` VALUES ('2', '1', '3');
INSERT INTO `tbl_tour_destination` VALUES ('3', '1', '6');

-- ----------------------------
-- Table structure for tbl_tour_details
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_details`;
CREATE TABLE `tbl_tour_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `information` text CHARACTER SET utf8,
  `itinerary` text,
  `include` text CHARACTER SET utf8,
  `exclude` text CHARACTER SET utf8,
  `gallery` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `tour` (`tour_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour_details
-- ----------------------------
INSERT INTO `tbl_tour_details` VALUES ('1', '1', '', '[{\"title\":\"K\\u1ebf ho\\u1ea1ch ng\\u00e0y 1\",\"desc\":\"\"},{\"title\":\"K\\u1ebf ho\\u1ea1ch ng\\u00e0y 2\",\"desc\":\"\"}]', '', '', 'http://localhost/touristment/uploads/images/424.jpg,http://localhost/touristment/uploads/images/423.jpg,http://localhost/touristment/uploads/images/422.jpg,http://localhost/touristment/uploads/images/421.jpg,http://localhost/touristment/uploads/images/420.jpg');

-- ----------------------------
-- Table structure for tbl_tour_order
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_order`;
CREATE TABLE `tbl_tour_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `departure_id` int(11) NOT NULL,
  `slot_adult` int(11) NOT NULL,
  `slot_kid` int(11) NOT NULL,
  `slot_baby` int(11) NOT NULL,
  `total_price` decimal(10,0) NOT NULL,
  `custom_note` text,
  `status` tinyint(4) DEFAULT '0' COMMENT '0-chưa xử lý, 1-confirm, 2-paid, 3-cancel',
  `update_by` int(11) DEFAULT NULL,
  `note_by` text CHARACTER SET utf8,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer` (`customer_id`) USING BTREE,
  KEY `departure` (`departure_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour_order
-- ----------------------------
INSERT INTO `tbl_tour_order` VALUES ('1', '5', '1', '1', '1', '1', '0', 'jhhj', '1', '1', null, '1538280961', '1540825098');
INSERT INTO `tbl_tour_order` VALUES ('2', '6', '2', '1', '0', '0', '0', '?as', '1', '1', null, '1540391910', '1540825096');
INSERT INTO `tbl_tour_order` VALUES ('3', '7', '1', '2', '0', '0', '0', '', '1', '1', null, '1540481810', '1540825092');
INSERT INTO `tbl_tour_order` VALUES ('4', '8', '3', '1', '0', '0', '0', '', '3', '1', null, '1540481848', '1540825165');
INSERT INTO `tbl_tour_order` VALUES ('5', '9', '2', '1', '0', '0', '10000', '', '1', '1', null, '1540484829', '1540825090');
INSERT INTO `tbl_tour_order` VALUES ('6', '10', '1', '1', '0', '0', '10000', '', '3', '1', null, '1540484931', '1540825176');

-- ----------------------------
-- Table structure for tbl_tour_reviews
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tour_reviews`;
CREATE TABLE `tbl_tour_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `object_table` varchar(20) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `star` decimal(10,0) NOT NULL,
  `content` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_tour_reviews
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_town
-- ----------------------------
DROP TABLE IF EXISTS `tbl_town`;
CREATE TABLE `tbl_town` (
  `id` varchar(5) CHARACTER SET utf8 NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 NOT NULL,
  `disid` varchar(5) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `district_id` (`disid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('1', 'admin', '', 'phanthanh.humg@gmail.com', null, '$2y$13$LGwowjKx60I9MVMfgGnNruW6EF4ObiiMi3W5YxoB/NiLkpG0rcNFi', 'OYXxX1ekKYYT__vdZFfFAV6ss8qTdu34', '', '0', '0', '1536464563', '1536464563', '10');
