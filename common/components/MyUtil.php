<?php

namespace common\components;

use yii\helpers\Url;

class MyUtil
{
    public static function isExist($array, $key, $value='')
    {
        return isset($array[$key]) ? $array[$key] : $value;
    }

    public static function baseUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    }
}
