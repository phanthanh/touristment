<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Option extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $_configs;

    public static function tableName()
    {
        return 'tbl_options';
    }

    public static function getAll()
    {
        if(!$this->_configs) {
            $this->_configs = self::findAll();
        }
        return $this->_configs;
    }

    public function configMenu($data)
    {
        if(isset($data['menu_name'])) {
            $menu = self::findOne(['name'=>$data['menu_name']]);
            if(!$menu) {
                $menu = new self;
                $menu->name = $data['menu_name'];
            }
            $menu->value = $data['menu_value'];
            return($menu->save());
        }
        return false;
    }

    public function getSlider()
    {
        $slider = self::findOne(['name'=>'slider']);
        if($slider) {
            return json_decode($slider->value, true);
        }
        return null;
    }

    public function updateSlider($data)
    {
        $slider = self::findOne(['name'=>'slider']);
        if(!$slider) {
            $slider = new self;
            $slider->name = 'slider';
        }
        $slider->value = json_encode($data);
        return $slider->save();
    }

    public function getLogo()
    {
        $logo = self::find()->where(['name'=>'logo'])->one();
        if($logo) {
            return json_decode($logo->value, true);
        }
        return null;
    }

    public function updateLogo($data)
    {
        $logo = self::findOne(['name'=>'logo']);
        if(!$logo) {
            $logo = new self;
            $logo->name = 'logo';
        }
        $logo->value = json_encode($data);
        return $logo->save();
    }
}