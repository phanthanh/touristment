<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Category;
use common\models\Tag;

class Article extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName()
    {
        return 'tbl_article';
    }

    public static function categoryTableName()
    {
        return 'tbl_category';
    }

    public function getByFilters($search)
    {
        $query = (new Query)
            ->select('a.id, a.title, a.alias, a.content, a.intro_text, a.image, a.publish_up, a.featured, a.create_time, u.fullname autor, c.id cat_id, c.title cat_title, c.alias cat_alias')
            ->from(self::tableName() .' a')
            ->leftJoin('tbl_user u', 'u.id = a.create_by')
            ->leftJoin(self::categoryTableName() .' c', 'c.id = a.cat_id');
        if(isset($search['keyword']) && trim($search['keyword']) != '') {
            $query->andWhere(['OR', 
                ['LIKE', 'a.title', trim($search['keyword'])],
                ['LIKE', 'a.content', trim($search['keyword'])],
                ['LIKE', 'a.intro_text', trim($search['keyword'])],

                ['LIKE', 'c.title', trim($search['keyword'])],
            ]);
        }
        if(isset($search['cat_id']) && $search['cat_id']) {
            $cat_ids = (new Category)->getChildCatByParentId($search['cat_id'], false, true);
            $query->andWhere(['IN', 'cat_id', $cat_ids]);
        }
        if(isset($search['status']) && $search['status'] > -1) {
            $query->andWhere('a.status = :status', [':status'=>$search['status']]); 
        }
        else {
            $query->andWhere(['a.status'=>self::STATUS_ACTIVE]);
        }
        return $query;
    }

    public function edit($article_id, $data)
    {
        if($article_id) {
            $model = self::findOne($article_id);
        }
        else {
            $model = new self;
        }
        if(isset($data['cat_id'])) {
            $model->cat_id = $data['cat_id'];
        }
        if(isset($data['title'])) {
            $model->title = $data['title'];
        }
        // if(isset($data['intro_text'])) {
        //     $model->intro_text = $data['intro_text'];
        // }
        if(isset($data['content'])) {
            $model->content = $data['content'];
            $model->intro_text = substr($data['content'], 0, 150);
        }
        if(isset($data['image'])) {
            $model->image = $data['image'];
        }
        if(isset($data['publish_up']) && $data['publish_up']) {
            $model->publish_up = date('Y-m-d', strtotime($data['publish_up']));
        }
        if(isset($data['publish_down']) && $data['publish_down']) {
            $model->publish_down = date('Y-m-d', strtotime($data['publish_down']));
        }
        if(isset($data['status'])) {
            $model->status = $data['status'];
        }
        if(isset($data['featured'])) {
            $model->featured = $data['featured'];
        }
        if(isset($data['hit'])) {
            $model->hit = $data['hit'];
        }
        $model->create_by = Yii::$app->user->identity->id;
        $model->create_time = time();

        if($model->save() && $data['article_tags']) {
            // Save tag
            $tags = explode(',', $data['article_tags']);
            (new Tag)->createTagObjects($model->id, self::tableName(), $tags);
        }
        return $model;
    }

    public function getDetail($article_id)
    {
        $article = (new Query)
            ->select('a.id, a.title, a.cat_id, a.intro_text, a.content, a.image, a.publish_up, a.create_time, u.fullname autor, c.title cat_title, c.alias cat_alias')
            ->from('tbl_article a')
            ->leftJoin('tbl_user u', 'u.id = a.create_by')
            ->leftJoin('tbl_category c', 'c.id = a.cat_id')
            ->where('a.id = :aid', [':aid'=>$article_id])
            ->one();

        return $article;
    }

    public function getLatestNews($limit=10)
    {
        $articles = (new Query)
                ->select(['a.id', 'a.title', 'a.alias', 'a.intro_text', 'a.image', 'a.publish_up', 'c.id as cat_id', 'c.title as cat_title', 'c.alias as cat_alias'])
                ->from(self::tableName() .' a')
                ->leftJoin(self::categoryTableName() .' c', 'c.id = a.cat_id')
                ->where('a.status=1')
                ->limit($limit)
                ->orderBy([
                    'a.publish_up'=>SORT_DESC,
                    'a.create_time'=>SORT_DESC,
                    ])
                ->all();
        return $articles;
    }
}