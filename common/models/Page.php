<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class Page extends ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_page';
    }
}