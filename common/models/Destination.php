<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Destination extends ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_destination';
    }

    /*
      * Return query
    */ 
    public function getByFilters($search)
    {
        $query = self::find();
        if(isset($search['keyword']) && trim($search['keyword'])!='') {
            $query->andWhere(['LIKE', 'name', $search['keyword']]);
        }
        if(isset($search['sort']) && $search['sort']) {
            $query->orderBy($search['sort']);
        }
        return $query;
    }

    public function getOne($tour_id)
    {
        $query = (new Query);
        
    }

    public function getDestinations($array_ids=array())
    {
        $destinations = self::find()
                ->select('id, name, alias, image')
                ->where(['IN', 'id', $array_ids])
                ->orderBy(['id' => $array_ids])
                ->all();
        return $destinations;
    }

}