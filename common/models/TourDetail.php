<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\TourCategory;

class TourDetail extends ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_tour_details';
    }

    public function create($tour_id, $data_tour)
    {
        $model = self::findOne(['tour_id'=>$tour_id]);
        if(!$model) {
            $model = new self;
            $model->tour_id = $tour_id;
        }
        $model->itinerary = json_encode($data_tour['itinerary']);
        $model->information = $data_tour['information'];
        $model->include = $data_tour['include'];
        $model->exclude = $data_tour['exclude'];
        $model->gallery = $data_tour['gallery'];
        return $model->save();
    }
}