<?php
namespace common\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Category;
use common\models\TourDeparture;
use common\models\TourDetail;
use common\models\Destination;
use common\models\Common;

class Tour extends ActiveRecord
{

    const HANOI = 'hanoi';
    const DANANG = 'danang';
    const HOCHIMINH = 'hcm';

    public static function tableName()
    {
        return 'tbl_tour';
    }

    /*
      * Return query
    */ 
    public function getByFilter($search)
    {
        $query = self::find();
        if(isset($search['departure_date']) && $search['departure_date']) {
            $query->leftJoin('tbl_tour_departures d', self::tableName() .'.id = d.tour_id');
        }
        if(isset($search['cat_id']) && $search['cat_id']) {
            $tourCatModel = new Category;
            $cats = $tourCatModel->getChildCatByParentId($search['cat_id'], true, true);
            $query->andWhere(['IN', 'cat_id', $search['cat_id']]);
        }
        if(isset($search['departure']) && $search['departure']) {
            $query->andWhere(['departure_location'=>$search['departure']]);
        }
        if(isset($search['departure_date']) && $search['departure_date']) {
            $departure_date = date('Y-m-d', strtotime($search['departure_date']));
            $query->andWhere(['>=', 'start_date', $departure_date]);
        }
        if(isset($search['destination']) && $search['destination']) {
            $arr_tours = $this->getToursBydDestinationKeyword($search['destination']);
            $query->andWhere(['IN', self::tableName() .'.id', $arr_tours]);
        }
        if(isset($search['duration']) && $search['duration']) {
            if(isset($search['duration']['from']) && $search['duration']['from']) {
                $query->andWhere(['>=', 'duration_days', $search['duration']['from']]);
            }
            if(isset($search['duration']['to']) && $search['duration']['to']) {
                $query->andWhere(['<=', 'duration_days', $search['duration']['to']]);
            }
        }
        if(isset($search['price']) && $search['price']) {
            $query->andWhere(['OR', 
                    ['AND', 
                        ['>=', new Expression("JSON_EXTRACT(". self::tableName() .".price, '$.base')"), $search['price']['from']], 
                        ['<=', new Expression("JSON_EXTRACT(". self::tableName() .".price, '$.base')"), $search['price']['to']]],
                    ['AND', 
                        ['>=', new Expression("JSON_EXTRACT(". self::tableName() .".price, '$.sale')"), $search['price']['from']], 
                        ['<=', new Expression("JSON_EXTRACT(". self::tableName() .".price, '$.sale')"), $search['price']['to']]]
                    ]);
        }
        if(isset($search['star']) && $search['star']) {
            $query->andWhere(['IN', new Expression("JSON_EXTRACT(". self::tableName() .".rating, '$.star')"), $search['star']]);
        }
        if(isset($search['featured'])) {
            $query->andWhere(['featured'=>$search['featured']]);
        }
        // $query->groupBy('tbl_tour.id');
        if(isset($search['sort']) && $search['sort']) {
            $query->orderBy($search['sort']);
        }
        return $query;
    }

    public function getOne($tour_id)
    {
        $query = (new Query);
        
    }

    public function create($data)
    {
        $tourModel = new self;
        $tourModel->title = $data['title'];
        $tourModel->alias = Common::convertToAlias($data['title']);
        $tourModel->intro_text = $data['intro_text'];
        $tourModel->departure_location = $data['departure_location'];
        $tourModel->destination = $data['destination'];
        $departure_date = $departure['departure_date'];
        if(strpos($departure_date, '/') > -1) {
            $arr_date = explode('/', $departure_date);
            $departure_date = $arr_date[2]. '-' .$arr_date[0]. '-' .$arr_date[1];
        }
        $tourModel->duration_days = $data['duration_days'];
        $tourModel->duration_nights = $data['duration_nights'];
        $tourModel->price = $data['price'];
        $tourModel->sale_price = $data['sale_price'];
        $tourModel->images = json_encode($data['images']);
        $tourModel->featured = $data['featured'];
        $tourModel->status = $data['status'];
        $tourModel->create_time = time();
        $tourModel->create_by = Yii::$app->user->identity->id;

        if($tourModel->save()) {
            $tour_id = $tourModel->id;
            // Create tour departures
            if(isset($data['departure'])) {
                (new TourDeparture)->create($tour_id, $data['departure']);
            }
            // Create tour details
            (new TourDetail)->create($tour_id, $data);
            return $tourModel->id;
        }
        return false;
    }

    public function edit($tour_id, $data)
    {
        $tourModel = self::findOne($tour_id);
        if(!$tourModel) return false;

        $tourModel->cat_id = $data['cat_id'];
        $tourModel->title = $data['title'];
        $tourModel->alias = Common::convertToAlias($data['title']);
        $tourModel->intro_text = $data['intro_text'];
        $tourModel->departure_location = $data['departure_location'];
        $tourModel->destination = $data['destination'];
        // $departure_date = $departure['departure_date'];
        // if(strpos($departure_date, '/') > -1) {
        //     $arr_date = explode('/', $departure_date);
        //     $departure_date = $arr_date[2]. '-' .$arr_date[0]. '-' .$arr_date[1];
        // }
        $tourModel->duration_days = $data['duration_days'];
        $tourModel->duration_nights = $data['duration_nights'];
        $tourModel->price = json_encode($data['price']);
        $tourModel->images = json_encode($data['images']);
        $tourModel->featured = $data['featured'];
        $tourModel->status = $data['status'];
        if($data['price']['sale'] > 0 && $data['price']['sale'] < $data['price']['base']) {
            $tourModel->sale = 1;
        }
        $tourModel->create_time = time();
        $tourModel->create_by = Yii::$app->user->identity->id;

        if($tourModel->save()) {
            $tour_id = $tourModel->id;
            // Create tour departures
            if(isset($data['departure'])) {
                (new TourDeparture)->create($tour_id, $data['departure']);
            }
            // Create tour details
            (new TourDetail)->create($tour_id, $data);
            return $tourModel->id;
        }
        return false;
    }

    public function getSaleTours($limit=10)
    {
        $query = self::find()
                ->where('sale = 1')
                ->limit($limit)
                ->orderBy(['create_time'=>SORT_DESC])
                ->all();
        return $query;
    }

    public function getDetail($tour_id)
    {
        $tour = self::findOne($tour_id);
        if($tour) {
            $tour = $tour->attributes;
            $array_destinations = explode(',', $tour['destination']);
            $tour['destinations'] = (new Destination)->getDestinations($array_destinations);
            $tour['details'] = TourDetail::findOne(['tour_id'=>$tour['id']]);
            $tour['departures'] = TourDeparture::findAll(['tour_id'=>$tour['id']]);
            return $tour;
        }
        return null;
    }

    public static function calcMoney($tour_id, $departure_price=false, $slot_adult=1, $slot_kid=0, $slot_baby=0)
    {
        $tour = self::findOne($tour_id);
        $price = json_decode($tour['price'], true);
        if($departure_price) {
            $price_unit = $departure_price;
        }
        else {
            $price_unit = ($price['sale'] > 0 && $price['sale'] < $price['base']) ? $price['sale'] : $price['base'];
        }
        $total = $price_unit*$slot_adult + $price['kid']*$slot_kid + $price['baby']*$slot_baby;
        return $total;
    }

    public static function getDepartureName($code)
    {
        switch ($code) {
            case self::HANOI:
                return 'Hà Nội';
            case self::DANANG:
                return 'Đà Nẵng';
            case self::HOCHIMINH:
                return 'Tp.Hồ Chí Minh';
        }
        return null;
    }

    public function getToursBydDestinationKeyword($destination_keyword)
    {
        $query = (new Query)
                ->select('tour_id')
                ->from('tbl_destination d')
                ->leftJoin('tbl_tour_destination l', 'd.id = l.destination_id')
                ->where(['LIKE', 'd.name', $destination_keyword])
                ->column();
        return $query;
    }

    
    public function getToursByDestinationId($des_id)
    {
        $query = (new Query)
            ->select('t.*')
            ->from('tbl_tour_destination td')
            ->leftJoin('tbl_tour t', 't.id = td.tour_id')
            ->where('td.destination_id = :did', [':did' => $des_id])
            ->andWhere('t.status=1')
            ->orderBy('create_time desc');
            
        return $query;
    }
}