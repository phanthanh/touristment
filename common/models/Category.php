<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{   

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_TOUR = 'tour';
    const TYPE_ARTICLE = 'article';

    const PREFIX_PARENT = '-';

    public static function tableName()
    {
        return 'tbl_category';
    }

    public function getChildCatByParentId($cat_id=0, $type='', $filter_active=true, $only_get_id=false)
    {
        $query = self::find();
        if($only_get_id) {
            $query->select('id');
        }
        if($cat_id) {
            $parent = $cat_id . '-%';
            $query->andWhere(['LIKE', 'path', $parent]);
        }
        if($type) {
            $query->andWhere(['type'=>$type]);
        }
        if($filter_active) {
            $query->andWhere(['status'=>self::STATUS_ACTIVE]);
        }
        $query->orderBy(['path'=>SORT_ASC]);

        if($only_get_id) {
            $cats = $query->column();
        }
        else {
            $cats = $query->all();
        }

        return $cats;
    }

    public function edit($data, $id=false)
    {   
        // Update or Create
        $model = $id ? self::findOne($id) : new self;

        if(isset($data['title'])) {
            $model->title = $data['title'];
        }

        if (isset($data['description'])) {
            $model->description = $data['description'];
        }

        if (isset($data['featured'])) {
            $model->featured = $data['featured'];
        }

        if (isset($data['image'])) {
            $model->image = $data['image'];
        }
        
        $model->type = isset($data['type']) ? $data['type'] : self::TYPE_TOUR;
        $model->status = isset($data['status']) ? $data['status'] : self::STATUS_ACTIVE;

        if($model->save()) {
            // Path
            $parentCat = isset($data['cat_id']) ? self::findOne($data['cat_id']) : NULL;
            $old_path = $model->path;
            $new_path = $parentCat ? ($parentCat->path . str_pad($model->id, 3, '0', STR_PAD_LEFT) . self::PREFIX_PARENT) : (str_pad($model->id, 3, '0', STR_PAD_LEFT) . self::PREFIX_PARENT);
            $model->path = $new_path;

            // Update path of childs
            if( $new_path != $old_path) {
                $this->updateTreeParentChild($old_path, $new_path);
            }
            return $model->save();
        }
        return false;
    }

    public function getParentCatByPath($id, $path)
    {
        $len = strlen($path);
        $count = strlen(str_pad($id, 3, '0', STR_PAD_LEFT)) + 1;
        $path_parent = substr($path, 0, $len-$count);
        var_dump($path);
        if($path_parent) {
            $cat = self::findOne(['path'=>$path_parent]);
            return $cat;
        }
        return null;
    }

    public function updateTreeParentChild($old_path, $new_path)
    {
        $con = Yii::$app->db;
        $sql = 'UPDATE '. self::tableName() .' SET path = REPLACE(path, "'. $old_path .'", "'. $new_path .'") WHERE path LIKE "'. $old_path .'%"';
        $command = $con->createCommand($sql);
        return $command->execute();
    }
}