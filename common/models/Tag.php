<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class Tag extends ActiveRecord
{

    public static function tableName()
    {
        return 'tbl_tag';
    }

    public static function tableTagObjectName()
    {
        return 'tbl_tag_object';
    }

    /*
      * Return query
    */ 
    public function getByFilter($search)
    {
        $query = self::find();
        if(isset($search['object_id']) && $search['object_id']) {

        }
        if(isset($search['name']) && $search['name']) {
            $query->andWhere(['LIKE', 'name', $search['name']]);
        }
        if(isset($search['sort']) && $search['sort']) {
            $query->orderBy($search['sort']);
        }
        else {
            $query->orderBy('id DESC');
        }
        return $query;
    }

    public function createTagJsonFile()
    {
        $tags = self::find()->all();
        $data = array();
        foreach ($tags as $tag ) {
            array_push($data, ['value'=>$tag['id'], 'text'=>$tag['name']]);
        }
        $fp = fopen('json/tags.json', 'w');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

    public function getTagsByObject($objectId, $table)
    {
        $tags = (new Query)
            ->select('t.*')
            ->from(self::tableName() . ' t')
            ->leftJoin(self::tableTagObjectName() . ' to', 't.id = to.tag_id')
            ->where(['object_id'=>$objectId])
            ->andWhere(['object_table'=>$table])
            ->all();
        return $tags;
    }

    public function createTagObjects($objectId, $objectTable, $tags=array())
    {
        if(!$objectId || !$objectTable) return;
        foreach ($tags as $tagId) {
            $values[] = array($tagId, $objectId, $objectTable);
        }
        if(isset($values)) {
            Yii::$app->db->createCommand()->batchInsert(self::tableTagObjectName(), array('tag_id', 'object_id', 'object_table'), $values)->execute();
        }
        return;
    }
}