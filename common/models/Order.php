<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Customer;
use common\models\Tour;

class Order extends ActiveRecord
{
    const STATUS_WAITING = 0; // Đang chờ xác nhận
    const STATUS_CONFIRMED = 1; // Đã xác thực
    const STATUS_CANCEL = 2; // Hủy chuyến
    const STATUS_FINISHED = 3; // Hoàn tất chuyến đi

    public static function tableName()
    {
        return 'tbl_tour_order';
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_WAITING],
        ];
    }

    /*
      * Return query
    */ 
    public function getByFilter($search=array())
    {
        // $query = self::find();
        $query = (new Query)
                ->select('o.*, c.fullname, c.phone, c.email, c.address, d.tour_id, d.start_date, t.title, t.departure_location, d.price, d.sale_price')
                ->from(self::tableName() . ' o')
                ->leftJoin('tbl_customer c', 'c.id = o.customer_id')
                ->leftJoin('tbl_tour_departures d', 'd.id = o.departure_id')
                ->leftJoin('tbl_tour t', 't.id = d.tour_id');

        if(isset($search['order_id']) && $search['order_id']) {
            $query->andWhere('o.id = :oid', [':oid'=>$search['order_id']]);
        }
        if(isset($search['from_date']) && $search['from_date']) {
            $query->andWhere(['>', 'o.create_time', strtotime($search['from_date'])]);
        }
        if(isset($search['to_date']) && $search['to_date']) {
            $query->andWhere(['<', 'o.create_time', strtotime($search['to_date'])]);
        }
        if(isset($search['status']) && $search['status']>-1) {
            $query->andWhere('o.status=:status', [':status'=>$search['status']]);
        }
        if(isset($search['paid']) && $search['paid']>-1) {
            $query->andWhere('o.paid=:paid', [':paid'=>$search['paid']]);
        }
        $query->orderBy(['o.create_time'=>SORT_DESC]);
        return $query;
    }

    public function edit($order_id, $data)
    {
        $order = self::findOne($order_id);
        if(isset($data['status']) && $data['status'] != $order->status) {
            $order->status = $data['status'];
        }
        if(isset($data['paid']) && $data['paid'] != $order->paid) {
            $order->paid = $data['paid'];
        }
        if(isset($data['custom_note']) && trim($data['custom_note']) != '') {
            $order->custom_note = trim($data['custom_note']);
        }
        if(isset($data['admin_note']) && trim($data['admin_note']) != '') {
            $order->admin_note = trim($data['admin_note']);
        }
        $order->update_by = Yii::$app->user->identity->id;
        $order->update_time = time();
        return $order->save();
    }

    public function create($customer_id, $departure_id, $data)
    {
        if(!$customer_id || !$departure_id) return false;
        $departure = TourDeparture::findOne($departure_id);
        $model = new self;
        $model->customer_id = $customer_id;
        $model->departure_id = $departure_id;

        if(isset($data['slot_adult']) && $data['slot_adult']) {
            $model->slot_adult = $data['slot_adult'];
        }

        if(isset($data['slot_kid']) && $data['slot_kid']) {
            $model->slot_kid = $data['slot_kid'];
        }

        if(isset($data['slot_baby']) && $data['slot_baby']) {
            $model->slot_baby = $data['slot_baby'];
        }

        if(isset($data['custom_note'])) {
            $model->custom_note = $data['custom_note'];
        }
        
        if(isset($data['coupon'])) {
            $model->coupon  = $data['coupon'];
        }

        $price = $departure['sale_price'] ?: $departure['price'];
        $model->total_price = Tour::calcMoney(
            $departure['tour_id'], 
            $price, 
            $model->slot_adult, 
            $model->slot_kid, 
            $model->slot_baby
        );
        $model->create_time = time();
        return $model->save() ? $model->id : false;
    }
}