<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\TourCategory;
use common\models\TourDeparture;
use common\models\TourDetail;
use common\models\Destination;
use common\models\Common;

class Customer extends ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_customer';
    }

    public function create($data)
    {
        $model = new self;
        if(isset($data['phone'])) {
            $model->phone = $data['phone'];
        }
        if(isset($data['email'])) {
            $model->email = $data['email'];
        }
        if(isset($data['fullname'])) {
            $model->fullname = $data['fullname'];
        }
        if(isset($data['address'])) {
            $model->address = $data['address'];
        }
        $model->create_time = time();

        return $model->save() ? $model->id : false;
    }

}