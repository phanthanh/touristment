<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\TourCategory;
use common\models\Tour;

class TourDeparture extends ActiveRecord
{
    public static function tableName()
    {
        return 'tbl_tour_departures';
    }

    public function get($tour_id=0, $expired=false, $limit=10)
    {
        $query = self::find();
        if($tour_id) {
            $query->andWhere('tour_id=:tid', [':tid'=>$tour_id]);
        }
        if(!$expired) {
            $query->andWhere(['>=', 'start_date', date('Y-m-d')]);
        }
        $query->orderBy(['id'=>'ASC'])
                ->limit($limit);
        return $query->all();        
    }

    public function create($tour_id, $data_departures)
    {
        $data = [];
        foreach ($data_departures as $k => $departure) {
            if(isset($departure['id']) && $departure['id']) {
                $this->singleUpdate($departure['id'], $departure);
            }
            else {
                $data[$k]['tour_id'] = $tour_id;
                $start_date = strtotime($departure['start_date']);
                $start_date = date('Y-m-d', $start_date);
                $data[$k]['start_date'] = $start_date;
                $data[$k]['slots'] = $departure['slots'];
                $data[$k]['booked_slots'] = $departure['booked_slots'];
                $data[$k]['price'] = $departure['price'];
                $data[$k]['sale_price'] = $departure['sale_price'];
            }
        }
        if($data) {
            $columns = ['tour_id', 'start_date', 'slots', 'booked_slots', 'price', 'sale_price'];
            $command = Yii::$app->db->createCommand()->batchInsert(self::tableName(), $columns, $data);
            return $command->execute();
        }
        return true;
    }

    public function singleUpdate($departure_id, $data)
    {
        $departure = self::findOne($departure_id);
        if(!$departure) return false;

        $start_date = strtotime($data['start_date']);
        $start_date = date('Y-m-d', $start_date);
        $departure->start_date = $start_date;
        $departure->slots = $data['slots'];
        $departure->booked_slots = $data['booked_slots'];
        $departure->price = $data['price'];
        $departure->sale_price = $data['sale_price'];

        return $departure->save();
    }

    public function getTourByDepartureId($dep_id)
    {
        $departure = self::findOne($dep_id);
        if($departure) {
            return Tour::findOne($departure['tour_id']);
        }
        return null;
    }
}