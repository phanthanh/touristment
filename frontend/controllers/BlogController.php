<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use common\models\Article;
use common\models\Tour;
use common\models\ArticleCategory;
use yii\data\Pagination;

/**
 * Site controller
 */
class BlogController extends Controller
{
    public function actionIndex()
    {
        $search = [
            'sort' => [
                'featured' => 'SORT_DESC',
                'publish_up' => 'SORT_DESC'
            ]
        ];
        $query = (new Article)->getByFilters($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        
        $tours = (new Tour)->getSaleTours();
        return $this->render('index', [
            'items' => $items,
            'tours' => $tours,
            'pages' => $pages
        ]);
    }

    public function actionCategory()
    {
        $cat_id = Yii::$app->request->get('cat_id');
        $search = [
            'cat_id' => $cat_id,
            'sort' => [
                'publish_up' => 'SORT_DESC'
            ]
        ];
        $query = (new Article)->getByFilters($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
                
        return $this->render('category', [
            'items' => $items,
            'pages' => $pages
        ]);
    }

    public function actionDetail()
    {
        $article_id = Yii::$app->request->get('article_id');
        $article = (new Article)->getDetail($article_id);
        if(!$article) {
            return $this->redirect(Url::toRoute('site/error'));
        }
        return $this->render('detail', [
            'article' => $article
        ]);
    }
}