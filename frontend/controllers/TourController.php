<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Tour;
use common\models\Order;
use common\models\Customer;
use common\models\TourDeparture;
use common\models\Category;
use common\models\Common;
use common\models\Destination;

/**
 * Site controller
 */
class TourController extends Controller
{

    public function actionCategory()
    {
        $cat_id = Yii::$app->request->get('id');
        $cat = Category::findOne($cat_id);

        if(!$cat) {
            return $this->redirect(Url::home());
        }

        $search = array('cat_id'=>$cat_id);
        $query = (new Tour)->getByFilter($search);
        
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('category', [
            'cat'=>$cat,
            'pages'=>$pages,
            'items'=>$items
        ]);
    }

    public function actionSearch()
    {
        $search = Yii::$app->request->get();
        $query = (new Tour)->getByFilter($search);
        
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('search', [
            'pages'=>$pages,
            'items'=>$items
        ]);
    }

    public function actionDetail()
    {
        $tour_id = Yii::$app->request->get('tour_id');
        $tour = (new Tour)->getDetail($tour_id);
        
        return $this->render('detail', [
                'tour'=>$tour
            ]);
    }

    public function actionDestination()
    {
        $destination_id = Yii::$app->request->get('destination_id');
        $destination = Destination::findOne($destination_id);
        $query = (new Tour)->getToursByDestinationId($destination_id);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        
        return $this->render('destination', [
            'pages'=>$pages,
            'items'=>$items,
            'destination'=>$destination
        ]);
    }

    public function actionBooking()
    {
        $errors = array();
        $departure_id = Yii::$app->request->get('departure_id');
        $departure = TourDeparture::findOne($departure_id);
        if(!$departure) {
            return $this->redirect(Url::home());
        }
        $tour = (new TourDeparture)->getTourByDepartureId($departure_id);
        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $customer_id = (new Customer)->create($data);
            $order_id = (new Order)->create($customer_id, $departure_id, $data);
            return $this->redirect(Url::toRoute(['tour/booking-success', 'order_id'=>$order_id]));
        }
        return $this->render('booking', [
            'errors'=>$errors,
            'departure'=>$departure,
            'tour'=>$tour
        ]);
    }

    public function actionBookingSuccess()
    {
        $order_id = Yii::$app->request->get('order_id');
        $query = (new Order)->getByFilter([
                'order_id'=>$order_id
            ]);
        $order = $query->one();
        return $this->render('booking-success', ['order'=>$order]);
    }

    // AJAX
    public function actionAjaxCalBookingAmount()
    {
        if(Yii::$app->request->isPost) {
            $params = Yii::$app->request->post();
            $departure_id = $params['departure_id'];
            $slot_adult = $params['slot_adult'];
            $slot_child = $params['slot_child'];
            $slot_babay = $params['slot_babay'];
            $departure = TourDeparture::findOne($departure_id);
            if($departure) {
                $price_unit = $departure['sale_price'] && $departure['sale_price'] < $departure['price'] ? $departure['sale_price'] : $departure['price'];
                $amount = Tour::calcMoney($departure['tour_id'], $price_unit, $slot_adult, $slot_child, $slot_babay);
                $amount = Common::formatVnd($amount);
                return $amount;
            }
        }
        return false;
    }
}
