const LOCAL_URL = 'http://localhost/touristment/';

$(document).ready(function(e){
    $('.booking_price').on('change', function(e) {
        var ideparture = parseInt($('#departure_id').val()),
            sadult = parseInt($('[name=slot_adult]').val()),
            schild = parseInt($('[name=slot_child]').val()),
            sbaby = parseInt($('[name=slot_baby]').val()),
            sremain = parseInt($('#remain_slots').text());
        if(sadult + schild + sbaby > sremain) {
            alert('Số chỗ còn nhận cho tour này là '+ sremain + '. Vui lòng chọn lại số lượng khách');
            $(this).find('option:first').prop('selected', true);
            return;
        }
        $.ajax({
            url: LOCAL_URL + 'tour/ajax-cal-booking-amount',
            type: 'POST',
            data: {
                slot_adult: sadult,
                slot_child: schild,
                slot_baby: sbaby,
                departure_id: ideparture
            },
            success:function(res) {
                $('#total_money, #total_pay').html(res);
            }
        })
    })
})