function bookTour() {
    $('#myTab a[href=#departure]').tab('show');
    $('html, body').animate({
        scrollTop: $('#myTab').offset().top 
    }, 1000);
}
$( document).ready(function() {
    $( ".datepicker" ).datepicker();
} );

function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}