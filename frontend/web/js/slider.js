// var swiper = new Swiper('.swiper-container', {
//     effect: 'coverflow',
//     grabCursor: true,
//     centeredSlides: true,
//     slidesPerView: 'auto',
//     coverflowEffect: {
//       rotate: 50,
//       stretch: 0,
//       depth: 100,
//       modifier: 1,
//       slideShadows : true,
//     },
//     autoplay: {
//         delay: 2500,
//         disableOnInteraction: false,
//     },
//     pagination: {
//       el: '.swiper-pagination',
//     },
// });

jQuery(document).ready(function ($) {
    var options = {
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
            $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
        }
    };                           
    var jssor_slider1 = new $JssorSlider$('jssor_1', options);

    //responsive code begin
    //remove responsive code if you don't want the slider scales
    //while window resizing
    function ScaleSlider() {
        var parentWidth = $('#jssor_1').parent().width();
        if (parentWidth) {
            jssor_slider1.$ScaleWidth(parentWidth);
        }
        else
            window.setTimeout(ScaleSlider, 30);
    }
    //Scale slider after document ready
    ScaleSlider();
                                    
    //Scale slider while window load/resize/orientationchange.
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    //responsive code end
});