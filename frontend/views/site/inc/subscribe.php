<div class="block-subscribe">
    <h4 class="title">Nhận tin khuyến mãi</h4>
    <div class="content">
        <p class="desc">Các deal du lịch giảm giá đến 60% sẽ được gửi đến bạn</p>
        <form action="" method="POST" class="frm-subscribe">
            <div class="wrap">
                <div class="email-wrap">
                    <input type="email" class="form-control" placeholder="Nhập email của bạn">
                </div>
                <div class="button-wrap">
                    <button type="submit" class="btn-submit"><i class="material-icons">send</i></button>
                </div>
            </div>
        </form>
    </div>
</div>