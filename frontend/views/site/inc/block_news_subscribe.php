<?php  
use yii\helpers\Url;
use yii\web\View;
use frontend\components\ArticleWidget;
?>
<div class="block news-subscribe">
    <div class="container">
        <div class="block-header">
            <h2 class="block-title">Cẩm nang du lịch</h2>
            <h5 class="block-subtitle"><a href="<?php echo Url::toRoute('blog/index') ?>">Xem tất cả <i class="fa fa-long-arrow-right"></i></a></h5>
        </div>
        <div class="block-body">
            <div class="row align-items-center">
                <div class="block-latest-news col-sm-8">
                    <div class="inner">
                        <div class="background-content"></div>
                        <div class="owl-carousel">
                            <?php foreach ($articles as $article) : ?>
    
                                <?php echo ArticleWidget::widget([
                                    'id' => $article['id'],
                                    'title' => $article['title'],
                                    'alias' => $article['alias'],
                                    'cat_id' => $article['cat_id'],
                                    'cat_title' => $article['cat_title'],
                                    'cat_alias' => $article['cat_alias'],
                                    'intro_text' => $article['intro_text'],
                                    'publish_up' => $article['publish_up'],
                                    'image' => $article['image']
                                ]) ?>
    
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="subscribe col-sm-4">
                    <?php echo $this->render('./subscribe') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $js = "$('.block-latest-news .owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        dots: false,
        nav: true
    })";
$this->registerJs($js, View::POS_READY);