<div class="block our-value">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="value-box">
                    <div class="value-img">
                        <img src="https://bestour.com.vn/uploads/tu-van-24-7.png" alt="">
                    </div>
                    <div class="value-content">
                        <h3 class="value-title">Best Service</h3>
                        <p>Tư vấn nhanh nhất, nhiệt tình<br>nhất, chính xác nhất</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="value-box">
                    <div class="value-img">
                        <img src="https://bestour.com.vn/uploads/chat-luong-hang-dau.png" alt="">
                    </div>
                    <div class="value-content">
                        <h3 class="value-title">Best Quality</h3>
                        <p>Sản phẩm cam kết chất lượng dẫn<br>đầu thị trường du lịch Việt Nam</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="value-box">
                    <div class="value-img">
                        <img src="https://bestour.com.vn/uploads/bao-hiem-dich-vu.png" alt="">
                    </div>
                    <div class="value-content">
                        <h3 class="value-title">Best Price </h3>
                        <p>Cam kết giá tốt nhất thị trường so<br>với cùng dòng sản phẩm</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>