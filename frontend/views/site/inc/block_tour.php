<?php  
use yii\helpers\Url;
use yii\web\View;
use frontend\components\TourWidget;
?>
<div class="block block-tour">
    <div class="container">
        <div class="block-header">
            <h2 class="block-title">Tour khuyến mại</h2>
            <h5 class="block-subtitle"><a href="<?php echo Url::toRoute('') ?>"><i class="fa fa-long-arrow-right"></i> Xem tất cả</a></h5>
        </div>
        <div class="block-body">
            <div class="owl-carousel">

                <?php foreach ($tours as $tour) : 
                    $images = json_decode($tour['images'], true);
                    $price = json_decode($tour['price'], true); ?>

                <?php echo TourWidget::widget([
                        'id' => $tour['id'],
                        'title' => $tour['title'],
                        'alias' => $tour['alias'],
                        'base_price' => $price['base'],
                        'sale_price' => $price['sale'],
                        'duration_days' => $tour['duration_days'],
                        'duration_nights' => $tour['duration_nights'],
                        'featured' => $tour['featured'],
                        'publish_up' => $tour['publish_up'],
                        'image' => $images['intro']
                    ]) ?>

                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<?php $js = "$('.block-tour .owl-carousel').owlCarousel({
        items: 3,
        margin: 30,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            }, 
            991: {
                item : 3
            }
        }
    })";
$this->registerJs($js, View::POS_READY);