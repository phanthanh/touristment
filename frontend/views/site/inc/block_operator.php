<?php 
use yii\helpers\Url;
use yii\web\View;
?>
<div class="block-operator">
    <div class="container-fluid">
        <ul class="owl-carousel">
            <li><img src="<?php echo Url::to('@web/images/thaiair.png') ?>" alt="Thai Air"></li>
            <li><img src="<?php echo Url::to('@web/images/emirates.png') ?>" alt="Emirates"></li>
            <li><img src="<?php echo Url::to('@web/images/jetstar.png') ?>" alt="Jetstar"></li>
            <li><img src="<?php echo Url::to('@web/images/vietnamairline.png') ?>" alt="Vietnam Airline"></li>
            <li><img src="<?php echo Url::to('@web/images/vietjetair.png') ?>" alt="Vietjet Air"></li>
            <li><img src="<?php echo Url::to('@web/images/cathaypacific.png') ?>" alt="Cathay Pacific"></li>
        </ul>
    </div>
</div>
<?php $js = "$('.block-operator .owl-carousel').owlCarousel({
        items: 6,
        margin: 30,
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            991: {
                items: 4
            },
            1200: {
                items: 6
            }
        }
    })";
$this->registerJs($js, View::POS_READY);