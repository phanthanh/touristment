<?php  
use yii\helpers\Url;
use yii\web\View;
use frontend\components\DestinationWidget;
?>
<div class="block block-destination">
    <div class="block-header">
        <div class="container">
            <h2 class="block-title">Điểm đến ưa thích</h2>
            <h5 class="block-subtitle"><a href="<?php echo Url::toRoute('') ?>">Xem tất cả <i class="fa fa-long-arrow-right"></i></a></h5>
        </div>
    </div>
    <div class="block-body">
    <?php if($destinations) : ?>
        <div class="owl-carousel">
        <?php foreach ($destinations as $destination) : ?>

            <?php echo DestinationWidget::widget([
                    'id' => $destination['id'],
                    'name' => $destination['name'],
                    'alias' => $destination['alias'],
                    'image' => $destination['image']
                ]) ?>

            <?php endforeach ?>
        </div>
    </div>
    <?php endif ?>
</div>
<?php $js = "$('.block-destination .owl-carousel').owlCarousel({
        items: 5,
        margin: 0,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            991: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    })";
$this->registerJs($js, View::POS_READY);