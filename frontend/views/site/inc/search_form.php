<?php  
use yii\helpers\Url;
?>
<div class="search-tour">
    <div class="container">
        <form action="<?php echo Url::toRoute('tour/search') ?>" method="GET">
            <div class="form-group">
                <select name="departure_location" class="form-control">
                    <option value="">Điểm khởi hành</option>
                    <option value="hanoi">Hà Nội</option>
                    <option value="danang">Đà Nẵng</option>
                    <option value="hochiminh">Hồ Chí Minh</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="arrival_location" class="form-control" placeholder="Điểm đến">
            </div>
            <div class="form-group">
                <input type="text" name="departure_date" class="form-control datepicker" autocomplete="off" placeholder="Ngày khởi hành">
            </div>
            <div class="form-group">
                <select name="duration" class="form-control">
                    <option value="">Thời gian tour</option>
                    <option value="1">1 Ngày</option>
                    <option value="2">2 Ngày</option>
                    <option value="3">3 Ngày</option>
                    <option value="7">1 Tuần</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit">Tìm kiếm</button>
            </div>
        </form>
    </div>
</div>