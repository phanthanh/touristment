<?php  
use yii\web\View;

// $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css');
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/jssor-slider/27.5.0/jssor.slider.min.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
// $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/slider.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
?>
<div class="main-slider">
    <div id="jssor_1" style="position:relative;top:0px;left:0px;width:1920px;height:600px;overflow:hidden;">
        <div data-u="slides" style="position:absolute;top:0px;left:0px;width:1920px;height:600px;overflow:hidden;">
        <?php foreach ($slider['source'] as $slide) : ?>
            <div class="swiper-slide" style="background: url(<?php echo $slide ?>) center" />
            </div>
        <?php endforeach ?>
        </div>
        <span u="arrowleft" class="jssora13l">
</span>
<!-- Arrow Right -->
<span u="arrowright" class="jssora13r"></span>
    </div>
</div>