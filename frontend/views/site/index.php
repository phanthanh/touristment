<?php  
use yii\web\View;

$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css');
?>

<div class="home-wrap">
    <?php echo $this->render('./inc/block_slider', ['slider'=>$slider]) ?>
    <?php echo $this->render('./inc/search_form') ?>
    <?php echo $this->render('./inc/top_banner') ?>
    <?php echo $this->render('./inc/block_tour', ['tours'=>$sale_tours]) ?>
    <?php echo $this->render('./inc/block_destination', ['destinations'=>$destinations]) ?>
    <?php echo $this->render('./inc/block_news_subscribe', ['articles'=>$latest_news]) ?>
    <?php echo $this->render('./inc/block_operator') ?>
</div>