<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Liên hệ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-wrap">
    <h1><?= Html::encode($this->title) ?></h1>

    <form action="" method="POST">
        <div class="form-group">
            <input type="text" name="fullname" class="form-control" placeholder="Họ và tên" required>
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Email" required>
        </div>
        <div class="form-group">
            <input type="text" name="phone" class="form-control" placeholder="Số điện thoại">
        </div>
        <div class="form-group">
            <textarea name="fullname" class="form-control" placeholder="Tin nhắn"></textarea>
        </div>
        <div class="form-group">
            <button type="submit">Gửi liên hệ</button>
        </div>
    </form>

    <div class="map">
    
    </div>
</div>
