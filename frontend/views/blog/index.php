<?php  
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use frontend\components\TourWidget;
// use kartik\social\FacebookPlugin; 
// use kartik\social\GooglePlugin;

$this->title = isset($cat) && $cat ? $cat['title'] : 'Cẩm nang du lịch';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breadcrumbs-wrap">
    <div class="container">
        <?php echo Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>
    </div>
</div>
<div class="blog-index_wrap container">
    <div class="row">
        <div class="col-sm-9">
            <div class="blog-header">
                <div class="grid-view">
                    <div class="view list_view active"><i class="material-icons">view_list</i></div>
                    <div class="view module_view"><i class="material-icons">view_module</i></div>
                </div>
            </div>
            <div class="blog-container">
                <?php foreach ($items as $item) {
                    echo $this->render('./item', ['article'=>$item]);
                } ?>
                <div class="blog-pagination">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
                
            </div>
        </div>
        <div class="col-sm-3">
            <div class="blog-right">
                <div class="box">
                    <h3 class="box-title">Tour nổi bật</h3>
                    <div class="content">
                        <?php foreach ($tours as $tour) : 
                            $images = json_decode($tour['images'], true);
                            $price = json_decode($tour['price'], true); ?>
            
                        <?php echo TourWidget::widget([
                                'id' => $tour['id'],
                                'title' => $tour['title'],
                                'alias' => $tour['alias'],
                                'base_price' => $price['base'],
                                'sale_price' => $price['sale'],
                                'duration_days' => $tour['duration_days'],
                                'duration_nights' => $tour['duration_nights'],
                                'featured' => $tour['featured'],
                                'image' => $images['intro'],
                                'custom_class' => 'quickview'
                            ]) ?>
            
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>