<?php  
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\social\FacebookPlugin; 
use kartik\social\GooglePlugin;

$this->title = $article['title'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breadcrumbs-wrap">
    <div class="container">
        <?php echo Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>
    </div>
</div>
<div class="blog-detail_wrap">
    <div class="blog-container container">
        <div class="blog-header">
                <h1 class="blog-title"><?php echo $article['title'] ?></h1>
                <?php if($article['image']) : ?>
                <div class="blog-media">
                    <img src="<?php echo $article['image'] ?>" alt="<?php echo $this->title ?>">
                </div>
                <?php endif ?>
                <div class="blog-info">
                    <span class="autor"><i class="fa fa-user"></i> <?php echo $article['autor'] ?></span>
                    <span class="category"><a href="<?php echo Url::toRoute(['blog/category', 'cat_id'=>$article['cat_id'], 'alias'=>$article['cat_alias']]) ?>"><i class="fa fa-folder-open"></i> <?php echo $article['cat_title'] ?></a></span>
                    <span class="publish_up"><i class="fa fa-calendar"></i> <?php echo date('H:i d/m', strtotime($article['publish_up'])) ?></span>
                </div>
        </div>
        <div class="blog-body">
            <div class="blog-content">
                <?php echo $article['content'] ?>
            </div>
        </div>
        <div class="blog-footer">
            <ul class="social">
                <li><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::LIKE, 'settings' => ['size'=>'small', 'layout'=>'button']]); ?></li>
                <li><?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::SHARE, 'settings' => ['size'=>'small', 'layout'=>'button_count', 'mobile_iframe'=>'false']]); ?></li>
                <li><?php echo GooglePlugin::widget(['pageId'=>'GOOGLE_PLUS_PAGE_ID']); ?></li>
            </ul>
            <div class="tags">
                <span class="tag-title"></span>
                <?php if(isset($tags) && $tags) : ?>
                    <?php foreach($tags as $tag) : ?>
                    <span class="tag-item"><a href="<?php echo Url::toRoute(['blog/tag', 'name'=>$tag['id'], 'alias'=>$tag['alias']]) ?>"><?php echo $tag['name'] ?></a></span>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>