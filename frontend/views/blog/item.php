<?php  
use yii\helpers\Url;

?>
<div class="blog-item">
    <?php if($article['image']) : ?>
    <div class="blog-media">
        <img src="<?php echo $article['image'] ?>" alt="<?php echo $this->title ?>">
    </div>
    <h1 class="blog-title"><a href="<?php echo Url::toRoute(['blog/detail', 'article_id'=>$article['id'], 'alias'=>$article['alias']]) ?>"><?php echo $article['title'] ?></a></h1>
    <?php endif ?>
    <div class="blog-info">
        <span class="autor"><i class="fa fa-user"></i> <?php echo $article['autor'] ?></span>
        <span class="category"><a href="<?php echo Url::toRoute(['blog/category', 'cat_id'=>$article['cat_id'], 'alias'=>$article['cat_alias']]) ?>"><i class="fa fa-folder-open"></i> <?php echo $article['cat_title'] ?></a></span>
        <span class="entry-date"><i class="material-icons">event_note</i>
            <span class="entry-day"><?php echo  date('d', strtotime($article['publish_up'])) ?></span> 
            <span class="entry-month">Tháng <?php echo  date('m', strtotime($article['publish_up'])) ?></span>
        </span>
    </div>
    <div class="blog-introtext">
        <?php echo $article['intro_text'] ?>
    </div>
</div>