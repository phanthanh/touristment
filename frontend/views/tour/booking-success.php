<?php  
use yii\helpers\Url;
use common\models\Common;
use common\models\Tour;
?>
<div class="booking_success-wrap">
    <div class="container">
        <div class="wrap">
            <div class="alert alert-success">
                <p>Quý khách đã đặt vé thành công cho chuyến du lịch.</p>
            </div>
            <div class="title"><i class="material-icons">thumb_up</i> Thông tin chuyến đi</div>
            <table class="table">
                <tr>
                    <td>Họ và tên:</td>
                    <td><?php echo $order['fullname'] ?></td>
                </tr>
                <tr>
                    <td>Số điện thoại:</td>
                    <td><?php echo $order['phone'] ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php echo $order['email'] ?></td>
                </tr>
                <tr>
                    <td>Địa chỉ:</td>
                    <td><?php echo $order['address'] ?></td>
                </tr>
                <tr>
                    <td>Chuyến đi:</td>
                    <td><?php echo $order['title'] ?></td>
                </tr>
                <tr>
                    <td>Điểm xuất phát:</td>
                    <td><?php echo Tour::getDepartureName($order['departure_location']) ?></td>
                </tr>
                <tr>
                    <td>Ngày khởi hành:</td>
                    <td><?php echo date('d/m/Y', strtotime($order['start_date'])) ?></td>
                </tr>
                <tr>
                    <td>Số chỗ đăng ký:</td>
                    <td><?php echo $order['slot_adult']+$order['slot_child']+$order['slot_baby'] ?></td>
                </tr>
                <tr>
                    <td>Tổng tiền:</td>
                    <td><?php echo Common::formatVnd($order['total_price']) ?></td>
                </tr>
                <tr>
                    <td>Trạng thái giao dịch:</td>
                    <td><span class="text-secondary">Chưa thanh toán</span></td>
                </tr>
            </table>
            <div class="back-link">
                <a href="<?php echo Url::home() ?>">Quay về</a>
            </div>
        </div>
    </div>
</div>