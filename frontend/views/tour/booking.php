<?php  
use yii\web\View;
use common\models\Common;

$this->registerJsFile('@web/js/booking.js', ['depends'=>[yii\web\JqueryAsset::className()]]);
$image = json_decode($tour['images'], true);
?>
<div class="booking-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="booking-info">
                    <div class="booking-title">
                        <h3>Thông tin liên hệ</h3>
                    </div>
                    <div class="booking-content">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label for="">Số điện thoại *</label>
                                <div class="info-item">
                                    <i class="material-icons">local_phone</i>
                                    <input type="text" name="phone" class="form-control" data-validate="phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Email *</label>
                                <div class="info-item">
                                    <i class="material-icons">email</i>
                                    <input type="text" name="email" class="form-control" data-validate="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Họ và tên *</label>
                                <div class="info-item">
                                    <i class="material-icons">person</i>        
                                    <input type="text" name="fullname" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Địa chỉ *</label>
                                <div class="info-item">
                                    <i class="material-icons">location_on</i>        
                                    <input type="text" name="address" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <?php $remain_slots = $departure['slots'] - $departure['booked_slots'] ?>
                                    <div class="col">
                                        <label for="">Số chỗ người lớn</label>
                                        <select name="slot_adult" class="form-control booking_price">
                                        <?php for($i=1; $i<=$remain_slots; $i++) : ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="">Số chỗ trẻ em</label>
                                        <select name="slot_child" class="form-control booking_price">
                                        <?php for($i=0; $i<=$remain_slots; $i++) : ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="">Số chỗ em bé</label>
                                        <select name="slot_baby" class="form-control booking_price">
                                        <?php for($i=0; $i<=$remain_slots; $i++) : ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="departure_id" value="<?php echo Yii::$app->request->get('departure_id') ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Yêu cầu khác</label>
                                <textarea class="form-control" name="custom_note"></textarea>
                            </div>
                            <div class="form-group text-center">
                                <p class="booking-note">(Vui lòng nhập thông tin chính xác, <strong>daytour</strong> sẽ liên hệ với bạn sớm nhất.)</p>
                                <button type="submit" class="btn btn-booking">Hoàn thành</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="booking-payment">
                    <div class="booking-title">
                        <h3>Thông tin thanh toán</h3>
                    </div>
                    <div class="quick-tour-info">
                        <div class="tour-media">
                            <img src="<?php echo $image['intro'] ?>" alt="<?php echo $tour['title'] ?>">
                        </div>
                        <div class="tour-detail">
                            <h3 class="tour-name"><?php echo $tour['title'] ?></h3>
                            <div class="detail-item"><i class="material-icons">calendar_today</i> Ngày khởi hành <?php echo date('d/m', strtotime($departure['start_date'])) ?></div>
                            <div class="detail-item"><i class="material-icons">timer</i> <?php echo $tour['duration_days'] ?> ngày <?php echo $tour['duration_nights'] ?> đêm</div>
                            <div class="detail-item"><i class="material-icons">people</i> Số chỗ còn lại <span id="remain_slots"><?php echo $departure['slots'] - $departure['booked_slots'] ?></span></div>
                            <hr>
                        </div>
                    </div>
                    <div class="payment-content">
                        <div class="form-group row-money">
                            <label for="">Đơn giá</label>
                            <span><?php echo Common::formatVnd($departure['sale_price'] ?: $departure['price']) ?></span>
                        </div>
                        <div class="form-group row-money">
                            <label for="">Tổng tiền</label>
                            <span id="total_money"><?php echo Common::formatVnd($departure['sale_price'] ?: $departure['price']) ?></span>
                        </div>
                        <div class="form-group">
                            <label for="">Mã giảm giá</label>
                            <input type="text" class="form-control" placeholder="Nhập mã giảm giá">
                        </div>
                        <hr>
                        <div class="form-group row-money">
                            <label for="">Thanh toán</label>
                            <span id="total_pay strong"><?php echo Common::formatVnd($departure['sale_price'] ?: $departure['price']) ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>