<div class="search-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?php echo $this->render('./inc/search_sidebar', ['destination'=>$destination]) ?>
            </div>
            <div class="col-sm-8">
                <div class="filter-results">
                    <?php echo $this->render('./inc/search_sort') ?>

                    <?php echo $this->render('./inc/search_results', ['items'=>$items]) ?>
                </div>
            </div>
        </div>
    </div>
</div>