<div class="category-wrap">
    <div class="category-header">
        <img src="<?php echo $cat['image'] ?>" alt="<?php echo $cat['title'] ?>" class="img-fluid">
    </div>
    <div class="category-body">
        <div class="container">
            <?php echo $this->render('./inc/search_sort') ?>
            <?php echo $this->render('./inc/search_results', ['items'=>$items]) ?>
        </div>
    </div>
</div>