<?php  

$price_from = !!Yii::$app->request->get('price') ? Yii::$app->request->queryParams['price']['from'] : 1000;
$price_to = !!Yii::$app->request->get('price') ? Yii::$app->request->queryParams['price']['to'] : 30000;
$duration_from = !!Yii::$app->request->get('duration') ? Yii::$app->request->queryParams['duration']['from'] : 1;
$duration_to = !!Yii::$app->request->get('duration') ? Yii::$app->request->queryParams['duration']['to'] : 15;
$star = Yii::$app->request->get('star') ?: array();

$search = array(
    'price_from' => $price_from,
    'price_to' => $price_to,
    'duration_from' => $duration_from,
    'duration_to' => $duration_to,
    'star' => $star
);
?>
<div class="search-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?php echo $this->render('./inc/search_sidebar', $search) ?>
            </div>
            <div class="col-sm-8">
                <div class="filter-results">
                    <?php echo $this->render('./inc/search_sort') ?>

                    <?php echo $this->render('./inc/search_results', ['items'=>$items]) ?>
                </div>
            </div>
        </div>
    </div>
</div>