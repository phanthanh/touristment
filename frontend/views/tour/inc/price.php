<?php 

use yii\helpers\Url;

$price_include = $tour['details']['include'];
$price_exclude = $tour['details']['exclude'];
?>
<div class="price-wrap">
    <div class="price-include">
        <h5 class="title text-success"><i class="material-icons">check_circle</i> Giá bao gồm</h5>
        <div class="content">
            <?php echo $price_exclude ?>
        </div>
    </div>
    <div class="price-exclude">
        <h5 class="title text-danger"><i class="material-icons">cancel</i> Giá không bao gồm</h5>
        <div class="content">
            <?php echo $price_exclude ?>
        </div>
    </div>
</div>