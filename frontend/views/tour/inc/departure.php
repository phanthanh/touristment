<?php 

use yii\helpers\Url;

$departures = $tour['departures']; ?>
<table class="table">
    <thead>
        <tr>
            <th>STT</th>
            <th>Ngày khởi hành</th>
            <th>Số chỗ còn lại</th>
            <th>Giá tour</th>
            <th>Đặt tour</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($departures as $k => $departure) : ?>

        <tr>
            <td><?php echo $k+1 ?></td>
            <td><?php echo $departure['start_date'] ?></td>
            <td><?php echo $departure['booked_slots'] ?>/<?php echo $departure['slots'] ?></td>
            <td><?php echo $departure['sale_price'] ? $departure['sale_price'] : $departure['price'] ?></td>
            <td><a href="<?php echo Url::toRoute(['tour/booking', 'departure_id'=>$departure['id'], 'alias'=>$tour['alias']]) ?>" class="btn btn-warning">Đặt tour</a></td>
        </tr>

        <?php endforeach ?>
    </tbody>
</table>