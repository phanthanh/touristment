<?php  
use frontend\components\TourWidget;
use common\models\TourDeparture;
use common\models\Destination;
?>
<div class="tours-list">
    <?php foreach ($items as $item) : 
        $prices = json_decode($item['price'], true);
        $images = json_decode($item['images'], true);
        $departures = (new TourDeparture)->get($item['id'], false, 2); 
        $arr_destination = explode(',', $item['destination']);
        $destinations = (new Destination)->getDestinations($arr_destination); ?>

        <?php echo TourWidget::widget([
                'id' => $item['id'],
                'title' => $item['title'],
                'intro_text' => $item['intro_text'],
                'alias' => $item['alias'],
                'base_price' => $prices['base'],
                'sale_price' => $prices['sale'],
                'duration_days' => $item['duration_days'],
                'duration_nights' => $item['duration_nights'],
                'featured' => $item['featured'],
                'publish_up' => $item['publish_up'],
                'image' => $images['intro'],
                'departures' => $departures,
                'destinations' => $destinations,
                'custom_class' => 'vertical'
            ]) ?>
    <?php endforeach ?>
</div>