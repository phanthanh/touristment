<?php  

use yii\web\View;
use yii\helpers\Url;

$this->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$this->registerJsFile('//code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<aside class="frm-search">
    <div class="filtertip">
        <i class="material-icons">search</i>Tìm thấy <strong>100</strong> kết quả
    </div>
    <form action="<?php echo Url::toRoute('tour/search') ?>" method="GET">
        <div class="filter-item">
            <label for="">Điểm khởi hành</label>
            <select name="departure" class="form-control">
                <option value="hanoi" <?php if(Yii::$app->request->get('departure')=='hanoi') echo 'selected' ?>>Hà Nội</option>
                <option value="danang" <?php if(Yii::$app->request->get('departure')=='danang') echo 'selected' ?>>Đà Nẵng</option>
                <option value="hcm" <?php if(Yii::$app->request->get('departure')=='hcm') echo 'selected' ?>>Tp.Hồ Chí Minh</option>
            </select>
        </div>
        <div class="filter-item">
            <label for="">Điểm đến</label>
            <input type="text" class="form-control" name="destination" placeholder="Điểm đến" value="<?php echo Yii::$app->request->get('destination', (isset($destination) ? $destination->name : '')) ?>">
        </div>
        <div class="filter-item">
            <label for="">Ngày khởi hành</label>
            <input type="text" class="form-control datepicker" name="departure_date" value="<?php echo Yii::$app->request->get('departure_date') ?>" autocomplete="off" placeholder="Ngày khởi hành">
        </div>
        <div class="filter-item">
            <label for="">Giá</label>
            <input type="text" id="price" readonly class="slide-range">
            <input type="hidden" name="price[from]" id="price_from" value="<?php echo $price_from ?>">
            <input type="hidden" name="price[to]" id="price_to" value="<?php echo $price_to ?>">
            <div id="price-range"></div>
        </div>
        <div class="filter-item">
            <label for="">Thời lượng</label>
            <input type="text" id="duration" readonly class="slide-range">
            <div id="duration-range"></div>
            <input type="hidden" name="duration[from]" id="duration_from" value="<?php echo $duration_from ?>">
            <input type="hidden" name="duration[to]" id="duration_to" value="<?php echo $duration_to ?>">
        </div>
        <div class="filter-item">
            <label for="">Đánh giá</label>
            <?php for ($i=5; $i>0; $i--) : ?>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="star[]" value="<?php echo $i ?>" id="rate-star_<?php echo $i ?>" <?php if(in_array($i, $star)) echo 'checked' ?>>
                    <label class="form-check-label" for="rate-star_<?php echo $i ?>">
                        <span class="rating-stars">
                            <?php for ($j=1; $j < 6; $j++) : ?>
                            <i class="material-icons <?php if($j <= $i) echo 'vote' ?>">star_rate</i>
                            <?php endfor ?>
                        <span> <?php echo $i ?> sao
                    </label>
                </div>
            <?php endfor ?>
        </div>
        <div class="filter-item">
            <button type="submit" class="btn btn-warning btn-search">Tìm kiếm</button>
        </div>
    </form>
</aside>
<?php
  $js = '$( function() {
    $( "#price-range" ).slider({
      range: true,
      min: 1000,
      max: 30000,
      values: [ '. $price_from .', '. $price_to .' ],
      slide: function( event, ui ) {
        $( "#price" ).val( numberWithCommas(ui.values[ 0 ]) + ",000đ - " + numberWithCommas(ui.values[ 1 ]) + ",000đ" );
        $("#price_from").val(ui.values[ 0 ]);
        $("#price_to").val(ui.values[ 1 ]);
      }
    });
    $( "#price" ).val(numberWithCommas($( "#price-range" ).slider( "values", 0 )) + ",000đ - " + numberWithCommas($( "#price-range" ).slider( "values", 1 )) + ",000đ" );
    $( "#duration-range" ).slider({
      range: true,
      min: 1,
      max: 10,
      values: [ '. $duration_from .', '. $duration_to .' ],
      slide: function( event, ui ) {
        $( "#duration" ).val( ui.values[ 0 ] + " ngày - " + ui.values[ 1 ] + " ngày" );
        $("#duration_from").val(ui.values[ 0 ]);
        $("#duration_from").val(ui.values[ 0 ]);
      }
    });
    $( "#duration" ).val( $( "#duration-range" ).slider( "values", 0 ) + " ngày - " + $( "#duration-range" ).slider( "values", 1 ) + " ngày" );
  } );';
  $this->registerJs($js, View::POS_READY);