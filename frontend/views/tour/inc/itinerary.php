<?php  
$itineraries = json_decode($tour['details']['itinerary'], true);
?>

<div class="itinerary-wrap">
    <div id="accordion">
        <?php foreach ($itineraries as $k => $itinerary) : ?>
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0" data-toggle="collapse" data-target="#itinerary<?php echo $k ?>" aria-expanded="true" aria-controls="itinerary<?php echo $k ?>">
                    <span>Ngày <?php echo $k+1 ?></span> &nbsp;&nbsp; <?php echo $itinerary['title'] ?>
                </h5>
            </div>

            <div id="itinerary<?php echo $k ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <?php echo $itinerary['desc'] ?>
                </div>
            </div>
        </div>
        
        <?php endforeach ?>
    </div>
</div>