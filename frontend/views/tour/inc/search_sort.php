<?php  

use yii\web\View;


$this->registerCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$this->registerJsFile('//code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="sort-by-section">
    <div class="sort-bar">
        <div class="sort-item">
            <h4 class="sort-by-title">Sắp xếp:</h4>
        </div>
        <div class="sort-item">
            <select name="sort_by[price]" class="form-control">
                <option value="">Giá cả</option>
                <option value="asc">Thấp đến cao</option>
                <option value="desc">Cao đến thấp</option>
            </select>
        </div>
        <div class="sort-item">
            <select name="sort_by[title]" class="form-control">
                <option value="">Tên</option>
                <option value="asc">A-Z</option>
                <option value="desc">Z-A</option>
            </select>
        </div>
        <div class="sort-item">
            <select name="sort_by[rating]" class="form-control">
                <option value="">Đánh giá</option>
                <option value="asc">Kém nhất</option>
                <option value="desc">Tốt nhất</option>
            </select>
        </div>
    </div>
</div>
<?php 
$js = "";
$this->registerJs($js, View::POS_READY);