<?php 

use yii\web\View;
use yii\widgets\Breadcrumbs;
use frontend\components\DestinationTagsWidget;

$this->title = $tour['title'];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$images = json_decode($tour['images'], true);
?>
<div class="tour_detail-wrap">
    <div class="banner" style="background-image: url(<?php echo $images['full'] ?>)"></div>
    <div class="breadcrumbs-wrap">
        <div class="container">
            <?php echo Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]); ?>
        </div>
    </div>
    <div class="tour_intro-wrap">
        <div class="container">
            <div class="wrap">
                <h1 class="tour-title"><?php echo $tour['title'] ?></h1>
                <div class="row">
                    <div class="col-sm-8 pr-0">

                        <?php if(isset($tour['details']['gallery'])) : 
                            $gallery = explode(',', $tour['details']['gallery']); ?>

                            <div class="tour-gallery">

                                <?php foreach ($gallery as $image) : ?>

                                    <div class="gallery-item"><img src="<?php echo $image ?>" alt="<?php echo $tour['title'] ?>"></div>
                                    
                                <?php endforeach ?>

                            </div>

                            <div class="tour-gallery-nav">

                                <?php foreach ($gallery as $image) : ?>

                                    <div class="gallery-item"><img src="<?php echo $image ?>" alt="<?php echo $tour['title'] ?>"></div>
                                    
                                <?php endforeach ?>

                            </div>

                        <?php endif ?>

                    </div>
                    <div class="col-sm-4 pl-0">
                        <div class="overview">
                            <?php $price = json_decode($tour['price'], true) ?>
                            <div class="tour-box tour-price <?php if($price['sale']) echo 'tour-sale' ?>">
                                <span class="box-title"><i class="fa fa-fw fa-money"></i> Giá: </span><?php if($price['sale']) echo '<span class="sale_price">'. number_format($price['sale'], 3, '.', '.') .'<sup>đ</sup></span>' ?></span>
                                <span class="base_price"><?php echo number_format($price['base'], 3, '.', '.') ?><sup>đ</sup></span>
                            </div>
                            <div class="tour-box tour-destination">
                                <p class="box-title"><i class="fa fa-map-marker"></i> Lịch trình:</p>
                                <?php echo DestinationTagsWidget::widget(['destinations'=>$tour['destinations']]); ?>
                            </div>
                            <div class="tour-box tour-duration">
                                <span class="box-title"><i class="fa fa-fw fa-clock-o"></i> Thời gian: </span>
                                <span><?php echo $tour['duration_days'] ?> ngày <?php echo $tour['duration_nights'] ?> đêm</span>
                            </div>
                            <div class="tour-box tour-departure">
                                <span class="box-title"><i class="fa fa-fw fa-calendar-check-o"></i> Ngày khởi hành: </span>
                                <span><?php echo $tour['duration_days'] ?> ngày <?php echo $tour['duration_nights'] ?> đêm</span>
                            </div>
                            <div class="book-button">
                                <a href="javascript:void(0)" class="button btn-tour-available" onClick="bookTour()">Đặt Tour</a>
                                <a href="javascript:void(0)" class="button btn-tour-custom">Tour tự chọn</a>
                            </div>
                        </div>
                        <!-- <div class="support">
                            <h3 class="title">Hỗ trợ 24/7</h3>
                            <div class="wrap">
                                <ul>
                                    <li><i class="fa fa-phone"></i> (+01) 234-567-8910</li>
                                    <li><i class="fa fa-envelope"></i> Support@Goto.com</li>
                                    <li><i class="fa fa-clock"> 08:00am ~ 05:30pm (Mon to Sat)</i></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="tour_info-wrap">
        <div class="container">
            <div class="tour-info row">
                <div class="col-3">
                    <ul class="nav flex-column nav-pills" id="myTab" role="tablist" aria-orientation="vertical">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#itinerary" role="tab"><i class="material-icons">schedule</i> Lịch trình tour</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#departure" role="tab"><i class="material-icons">today</i> Lịch khởi hành</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#price" role="tab"><i class="material-icons">money</i> Bảng giá</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#information" role="tab"><i class="material-icons">error_outline</i> Thông tin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#rating" role="tab"><i class="material-icons">thumb_up</i> Đánh giá</a>
                        </li>
                    </ul>
                </div>
                <div class="col-9">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="itinerary" role="tabpanel">
                            <?php echo $this->render('./inc/itinerary', ['tour'=>$tour]) ?>
                        </div>
                        <div class="tab-pane" id="departure" role="tabpanel">
                            <?php echo $this->render('./inc/departure', ['tour'=>$tour]) ?>
                        </div>
                        <div class="tab-pane" id="price" role="tabpanel">
                            <?php echo $this->render('./inc/price', ['tour'=>$tour]) ?>
                        </div>
                        <div class="tab-pane" id="information" role="tabpanel">
                            <?php echo $this->render('./inc/information', ['tour'=>$tour]) ?>
                        </div>
                        <div class="tab-pane" id="rating" role="tabpanel">
                            <?php echo $this->render('./inc/rating', ['tour'=>$tour]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$js = "$('.tour-gallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.tour-gallery-nav'
});
$('.tour-gallery-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.tour-gallery',
    focusOnSelect: true,
    autoplay: true,
});";
$this->registerJs($js, View::POS_READY);