<?php  
use yii\helpers\Url;
$logo = Yii::$app->config->get('logo', 'source');
?>
<header>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="hotline">
                        <p><i class="material-icons">phone</i> 0123-456-789</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <ul class="social">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-skype"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="row position-static">
                <div class="col-sm-3">
                    <div class="logo">
                        <a href="<?php echo Url::base() ?>"><img src="<?php echo $logo ?>" alt="Du lịch"></a>
                    </div>
                </div>
                <div class="col-sm-9 position-static">
                    <?php echo $this->render('./menu') ?>
                </div>
            </div>
        </div>
    </div>
</header>