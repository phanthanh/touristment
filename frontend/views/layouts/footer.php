<?php  
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<footer class="footer">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="footer-logo">
                        <img src="<?php  ?>" alt="">
                    </div>
                    <div class="footer-slogan">
                        <p></p>
                    </div>
                    <div class="footer-social">
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="title">Về Worldtour</h3>
                            <ul>
                                <li><a href="<?php echo Url::toRoute(['page/detail', 'page_id'=>1, 'alias'=>'gioi-thieu']) ?>">Giới thiệu</a></li>
                                <li><a href="">Liên hệ</a></li>
                                <li><a href="<?php echo Url::toRoute('blog/index') ?>">Tin tức</a></li>
                                <li><a href="">Hợp tác cùng Worldtour</a></li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <h3 class="title">Điều khoản</h3>
                            <ul>
                                <li><a href="">Điều khoản</a></li>
                                <li><a href="">Dịch vụ thanh toán</a></li>
                                <li><a href="">Chính sách hoàn hủy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h3 class="title">Liên hệ</h3>
                    <div class="fb-page" 
                        data-href="https://www.facebook.com/facebook" 
                        data-small-header="true" 
                        data-adapt-container-width="true" 
                        data-hide-cover="false" 
                        data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-copyright">
        <div class="container">
            <img src="<?php echo Url::to('@web/images/cards_all.png') ?>" alt="">
        </div>
    </div>
</footer>