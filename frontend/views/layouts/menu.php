<?php  
use frontend\components\MenuWidget;
?>
<nav class="navbar navbar-expand-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="main-menu">
        <?php echo MenuWidget::widget(['name'=>'main-menu']) ?>
    </div>
</nav>