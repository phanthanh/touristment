<?php 

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Url;

class TourWidget extends Widget
{
    public $id;
    public $title;
    public $intro_text;
    public $alias;
    public $base_price;
    public $sale_price;
    public $duration_days;
    public $duration_nights;
    public $featured;
    public $publish_up;
    public $image;
    public $new_from;
    public $departures;
    public $destinations;
    public $custom_class;
    public $rating;
    protected $label;

    public function init()
    {
        parent::init();
        if($this->new_from == null) {
            $this->new_from = 7;
        }
        if($this->custom_class == null) {
            $this->custom_class = 'horizontal';
        }
        if($this->rating == null) {
            $this->rating = array(
                'vote'=>0,
                'count'=>0
            );
        }
    }

    public function run()
    {
        $this->instance();
        return $this->buildLayout();
    }

    protected function instance()
    {
        if($this->featured==1) {
            $this->label = 'hot';
        }
        elseif ($this->publish_up) {
            $dateDiff = date_diff(date_create(), date_create($this->publish_up));
            if($dateDiff->days < $this->new_from) {
                $this->label = 'new';
            }
        }
        elseif($this->sale_price) {
            $this->label = 'discount';
        }
    }


    public function buildLayout()
    {
        $html = '<div class="tour-item '. $this->custom_class .' '. $this->label .'">';
        $html .= '<div class="wrap">';
        if($this->custom_class == 'vertical') {
            $html .= '<div class="tour-media w40">';
            $html .= '<div class="media-box">';
            $html .= $this->genMediaBlock();
            $html .= '</div>';
            $html .= $this->genLabel();
            $html .= '</div>';
            $html .= '<div class="tour-detail w60">';
            $html .= '<div class="w70">';
            $html .= $this->genTitleBlock();
            $html .= $this->genIntroTextBlock();
            $html .= $this->genDestinationsBlock();
            $html .= '</div>';
            $html .= '<div class="w30">';
            $html .= $this->genRating();
            $html .= $this->genPriceBook();
            $html .= $this->genDurationBlock();
            $html .= '</div>';
            $html .= '</div>';
        }
        else {
            $html .= '<div class="tour-media">';
            $html .= $this->genLabel();
            $html .= $this->genMediaBlock();
            $html .= '</div>';
            $html .= '<div class="tour-detail">';
            $html .= $this->genTitleBlock();
            $html .= $this->genRating();
            $html .= $this->genIntroTextBlock();
            $html .= $this->genDeparturesBlock();
            $html .= $this->genDestinationsBlock();
            $html .= $this->genPriceBook();
            $html .= $this->genDurationBlock();
            $html .= '</div>';
        }
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    protected function genLabel()
    {
        $label_html = '';
        if($this->label) {
            $label_html .= '<span class="'. $this->label .'">';
            if($this->label == 'discount') {
                $label_html .= '<span class="discount-text">Giảm '. round(($this->base_price - $this->sale_price)*100 / $this->base_price) . '%</span>';
            }
            $label_html .= '</span>';
        }

        return $label_html;
    }

    protected function genTitleBlock()
    {
        $block = '<h3 class="title"><a href="'. Url::toRoute(['tour/detail', 'tour_id'=>$this->id, 'alias'=>$this->alias]) .'">'. $this->title .'</a></h3>';
        return $block;
    }

    protected function genRating()
    {
        $star = $this->rating['count'] ? $this->rating['vote'] / $this->rating['count'] : 0;
        $block = '<div class="rating">';
        for ($i=1; $i <= 5; $i++) { 
            $sclass = '';
            if($star >= $i) $sclass = 'active';
            $block .= '<span class="star '. $sclass .'"><i class="fa fa-star"></i></span>';
        }
        $block .= ' <span class="rating-text">('. $this->rating['count'] .' đánh giá)</span>';
        $block .= '</div>';
        return $block;
    }

    protected function genIntroTextBlock()
    {
        $block = '';
        if($this->intro_text) {
            $block = '<div class="intro-text">'. $this->intro_text .'</div>';
        }
        return $block;
    }

    protected function genMediaBlock()
    {
        $block = '<img src="'. $this->image .'" alt="'. $this->title .'">';
        return $block;
    }

    protected function genDurationBlock()
    {
        $block = '<div class="tour-duration"><i class="material-icons">access_time</i> '. $this->duration_days .' ngày '. $this->duration_nights .' đêm</div>';
        return $block;
    }

    protected function genPriceBook()
    {
        $block = '<div class="tour-price">';
        $block .= '<p class="base_price">'. number_format($this->base_price, 3, '.', '.') .'<sup>đ</sup></p>';
        if($this->sale_price) {
            $block .= '<p class="sale_price">'. number_format($this->sale_price, 3, '.', '.') .'<sup>đ</sup></p>';
        }
        $block .= '</div>';
        return $block;
    }

    protected function genDeparturesBlock()
    {
        $block = '';
        if($this->departures) {
            $block .= '<div class="tour-departures">';
            foreach ($this->departures as $departure) {
                $block .= '<div class="departure-item">';
                $block .= '<span class="start-date">'. date('d/m', strtotime($departure['start_date'])) .'</span>';
                $block .= '<span class="booked-slots">'. ($departure['slots'] - $departure['booked_slots']) .'</span>';
                $block .= '</div>';
            }
            $block .= '</div>';
            return $block;
        }
        return $block;
    }

    protected function genDestinationsBlock()
    {
        $block = '';
        if($this->destinations) {
            $block .= '<div class="tour-destinations"><i class="fa fa-map-marker"></i> ';
            foreach ($this->destinations as $destination) {
                $block .= '<span><a href="'. Url::toRoute(['tour/destination', 'id'=>$destination['id'], 'alias'=>$destination['alias']]) .'">'. $destination['name'] .'</a></span>';
            }
            $block .= '</div>';
        }
        return $block;
    }
}


