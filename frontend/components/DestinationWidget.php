<?php 

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Url;

class DestinationWidget extends Widget
{
    public $id;
    public $name;
    public $alias;
    public $image;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $html = '<div class="destination-item">';
        $html .= '<div class="wrap">';
        $html .= '<div class="destination-media">';
        $html .= '<img src="'. $this->image .'" alt="'. $this->name .'">';
        $html .= '</div>';
        $html .= '<div class="destination-detail">';
        $html .= '<h3 class="title"><i class="material-icons">location_on</i>'. $this->name .'</h3>';
        $html .= '<div class="viewmore"><a href="'. Url::toRoute(['tour/destination', 'destination_id'=>$this->id, 'alias'=>$this->alias]) .'">Xem thêm</a></div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}


