<?php 
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models\Option;
 
class ConfigComponent extends Component
{
    protected $_configs;

    public function init()
    {
        parent::init();
        if(!$this->_configs) {
            $data = Option::findAll(['autoload'=>1]);
            foreach ($data as $item ) {
                $configs[$item->name] = json_decode($item->value, true);
            }
            $this->_configs = $configs;
        }
    }

    public function get($name='', $attribute='')
    {
        if(isset($this->_configs[$name])) {
            $config = $this->_configs[$name];
            if($attribute) {
                if(isset($config[$attribute])) {
                    return $config[$attribute];
                }
            } 
            return $this->_configs[$name];
        }
        return null;
    }
}