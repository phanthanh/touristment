<?php 

namespace frontend\components;

use yii\base\Widget;
use common\models\Option;

class MenuWidget extends Widget
{
    public $name;
    public $link;

    public function init()
    {
        parent::init();
        if($this->name===null) {
            return;
        }
    }

    public function run()
    {
        $menu = Option::findOne(['name'=>$this->name]);
        $menuVal = json_decode($menu->value, true);
        $html = '<nav class="menu '. $this->name .'">';
        $html .= $this->generateMenu($menuVal[0]);
        $html .= '</nav>';

        return $html;
    }

    public function generateMenu($menu)
    {
        $html = '<ul>';
        foreach ($menu as $item) {
            $active = $this->link == $item['link'] ? 'active' : '';
            $html .= '<li class="'. $active .'"><a href="'. $item['link'] .'">'. $item['title'] . '</a>';

            if(!empty($item['children'][0])) {
                $html .= '<b class="caret collapsed"></b>';
                $html .= $this->generateMenu($item['children'][0]);
            }

            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }
}


