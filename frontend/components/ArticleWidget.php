<?php 

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Url;

class ArticleWidget extends Widget
{
    public $id;
    public $title;
    public $alias;
    public $cat_id;
    public $cat_title;
    public $cat_alias;
    public $intro_text;
    public $publish_up;
    public $image;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $html = '<div class="item">';
        $html .= '<div class="wrap">';
        $html .= '<div class="article-detail">';
        $html .= '<h3 class="title"><a href="'. Url::toRoute(['blog/detail', 'article_id'=>$this->id]) .'">'. $this->title .'</a></h3>';
        $html .= '<div class="info">';
        $html .= '<span class="in-category"><a href="'. Url::toRoute(['blog/category', 'id'=>$this->cat_id, 'alias'=>$this->cat_alias]) .'"><i class="material-icons">folder_open</i> '. (isset($this->cat_title) ? $this->cat_title : '') .'</a></span>';
        $html .= '<span class="entry-date">';
        $html .= '<span class="entry-day">'. date('d', strtotime($this->publish_up)) .'</span>';
        $html .= '<span class="entry-month">Tháng '. date('m', strtotime($this->publish_up)) .'</span>';
        $html .= '</span>';
        $html .= '</div>';
        $html .= '<div class="introtext">';
        $html .= '<p>'. $this->intro_text .'</p>';
        $html .= '<p class="readmore"><a href="'. Url::toRoute(['blog/detail', 'article_id'=>$this->id]) .'">Xem thêm</a></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="article-media">';
        $html .= '<a href="'. Url::toRoute(['blog/detail', 'article_id'=>$this->id]) .'"><img src="'. $this->image .'" alt="'. $this->title .'"></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}


