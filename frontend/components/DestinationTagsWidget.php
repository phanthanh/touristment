<?php 

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Url;

class DestinationTagsWidget extends Widget
{
    public $destinations;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $html = '<ul class="destination-tags">';
        if($this->destinations) {
            foreach ($this->destinations as $destination ) {
                $html .= '<li><a href="'. Url::toRoute(['destination/index', 'id'=>$destination['id'], 'alias'=>$destination['alias']]) .'">'. $destination['name'] .'</a></li>';
            }
        }
        $html .= '</ul>';

        return $html;
    }
}
?>
