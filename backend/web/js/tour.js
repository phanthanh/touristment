function addItinerary() {
    var index = $('#itinerary-index').val();
    var next_index = parseInt(index) + 1;
    var next_index_label = parseInt(next_index) + 1;
    itinerary_item = '<div class="card">'
    itinerary_item += '<div class="card-header">'
    itinerary_item += '<h5 class="mb-0">'
    itinerary_item += '<span data-toggle="collapse" data-target="#tour-itinerary_'+ next_index +'" aria-expanded="true">'
    itinerary_item += '<span class="itinerary-label">Ngày '+ next_index_label +'</span>,'
    itinerary_item += '<span class="itinerary-label">Your Plan</span>'
    itinerary_item += '</span>'
    itinerary_item += '</h5>'
    itinerary_item += '</div>'
    itinerary_item += '<div id="tour-itinerary_'+ next_index +'" class="collapse show" data-parent="#accordion">'
    itinerary_item += '<div class="card-body">'
    itinerary_item += '<div class="form-group">'
    itinerary_item += '<label>Tiêu đề</label>'
    itinerary_item += '<input type="text" name="itinerary['+ next_index +'][title]" class="form-control" value="Kế hoạch ngày '+ next_index_label +'">'
    itinerary_item += '</div>'
    itinerary_item += '<div class="form-group">'
    itinerary_item += '<label>Mô tả</label>'
    itinerary_item += '<div class="tour-itinerary">'
    itinerary_item += '<textarea name="itinerary['+ next_index +'][desc]"></textarea>'
    itinerary_item += '</div>'
    itinerary_item += '</div>'
    itinerary_item += '</div>'
    itinerary_item += '</div>'
    itinerary_item += '</div>'
    $('.itinerary_block').append(itinerary_item);
    tinymceReload();
}

function addDeparture() {
    var index = $('#departure-index').val();
    var next_index = parseInt(index) + 1;
    departure_item = '<div class="card">'
    departure_item += '<div class="card-header" id="tour-departure_'+ next_index +'">'
    departure_item += '<h5 class="mb-0">'
    departure_item += '<span data-toggle="collapse" data-target="#tour-departure_'+ next_index +'" aria-expanded="true">'
    departure_item += 'Ngày <span class="departure-label">Your Plan</span>'
    departure_item += '</span>'
    departure_item += '</h5>'
    departure_item += '</div>'
    departure_item += '<div id="#tour-departure_'+ next_index +'" class="collapse show">'
    departure_item += '<div class="card-body">'
    departure_item += '<div class="form-group row">'
    departure_item += '<div class="col-auto">'
    departure_item += '<label>Ngày khởi hành</label>'
    departure_item += '<div class="input-group date">'
    departure_item += '<input type="text" class="form-control datetimepicker" name="departure['+ next_index +'][start_date]" />'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '<div class="form-group row">'
    departure_item += '<div class="col-auto">'
    departure_item += '<label>Số chỗ</label>'
    departure_item += '<div class="tour-departure">'
    departure_item += '<input type="number" name="departure['+ next_index +'][slots]" class="form-control" />'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '<div class="col-auto">'
    departure_item += '<label>Số chỗ còn lại</label>'
    departure_item += '<div class="tour-departure">'
    departure_item += '<input type="number" name="departure['+ next_index +'][booked_slots]" class="form-control" />'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '<div class="form-group row">'
    departure_item += '<div class="col-auto">'
    departure_item += '<label>Giá</label>'
    departure_item += '<div class="tour-departure">'
    departure_item += '<input type="text" name="departure['+ next_index +'][price]" class="form-control" />'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '<div class="col-auto">'
    departure_item += '<label>Giảm giá</label>'
    departure_item += '<div class="tour-departure">'
    departure_item += '<input type="text" name="departure['+ next_index +'][sale_price]" class="form-control" />'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    departure_item += '</div>'
    $('.departure_block').append(departure_item);
    $('.datetimepicker').datetimepicker({
        format: 'L'
    });
}
$(function () {
    $('.datetimepicker').datetimepicker({
        format: 'L'
    });
});
