var group = $(".menu >ul").sortable({
  group: 'serialization',
  onDrop: function ($item, container, _super) {
    $item.removeClass(container.group.options.draggedClass).removeAttr("style")
    $("body").removeClass(container.group.options.bodyClass)
    menuTrigger();
    _super($item, container);
  },
});

function menuTrigger() {
  var data = group.sortable("serialize").get();    
  var jsonString = JSON.stringify(data, null, '');
  $('#menu_value').val(jsonString);
}

$(document).ready(function(e){
  $('body').on('click', '.add_menu_item', function(e){
    var menuItems = '';
    $(this).closest('.menu_items_wrap').find('input:checked').each(function(i){
      var title = $(this).data('title'), 
        link = $(this).data('link');
      menuItems += genMenuItem(title, link);
    })
    if(menuItems) {
      $('body').find('nav.menu >ul').append(menuItems)
    }
    $(this).closest('.menu_items_wrap').find('input').prop('checked', false);
    menuTrigger();
  });

  /* Toggle menu settings */
  $('body').on('click', '.menu_item-setting', function(){
    $(this).closest('li').find('.menu_item-settings').first().toggle()
  })

  /* Change title menu item */
  $('body').on('input', '.setting_title', function(e){
    $(this).closest('li').find('.menu_item-title').first().text(this.value)
  })

  $('body').on('click', '.submit_delete', function(e){
    e.preventDefault()
    var $item = $(this).closest('li')
    $item.before($item.find('ul').first().children())
    $item.remove()
    var data = group.sortable("serialize").get();    
    var jsonString = JSON.stringify(data, null, '');
    $('#menu_value').val(jsonString);
  })
})

function genMenuItem(title, link) {
  var html = '<li class="menu-item" data-title="'+ title +'" data-link="'+ link +'">';
  html += '<div class="menu-item-bar">';
  html += '<div class="menu-item-handle ui-sortable-handle">';
  html += '<span class="item-title"><span class="menu_item-title">'+ title +'</span></span>';
  html += '<span class="item-controls">';
  html += '<a class="menu_item-setting" href="javascript:void(0)"><i class="fa fa-caret-down"></i></a>';
  html += '</span>';
  html += '</div>';
  html += '<div class="menu_item-settings">';
  html += '<label>Tên menu</label>';
  html += '<input type="text" value="'+ title +'" class="form-control setting_title">';
  html += '<div class="menu-item-actions">';
  html += '<a href="javascript:void(0)" class="submit_delete">Xóa</a>';
  html += ' | ';
  html += '<a href="javascript:void(0)" class="submit_cancel">Hủy</a>';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '<ul></ul>';
  html += '</li>';
  return html;
}

$('body').on('click', '.toggle_child_menu', function(){
  $(this).closest('li').find('ul').first().toggleClass('collapsed');
  if($(this).html() == 'chevron_left') {
    $(this).html('expand_more')
  }
  else if($(this).html() == 'expand_more') {
    $(this).html('chevron_left')
  }
})