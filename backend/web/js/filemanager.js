function responsive_filemanager_callback(field_id){
  var url = $('#'+field_id).val();
  var img_html = '';
  var is_multipe = $('#'+field_id).data('multiple');
  var multiple = url.match(/^\[(.+)\]$/);
  if(multiple) {
    var images = multiple[1].split(',');
    for (let i = 0; i < images.length; i++) {
      img_html += '<div class="gallery-item"><img src='+ images[i] +' /><span class="remove" data-id="'+ field_id +'">&times;</span></div>';
      if(is_multipe != true) {
        break;
      }
    }
  }
  else {
    img_html = '<div class="gallery-item"><img src="'+ url +'" /><span class="remove" data-id="'+ field_id +'">&times;</span></div>';
  }
  
  if(is_multipe != true) {
    $('#'+field_id).siblings('.image-preview').html(img_html);  
  }
  else {
    $('#'+field_id).siblings('.image-preview').append(img_html);
  }
  filemanagerCallback(field_id);
}

$('body').on('click', '.gallery-item .remove', function(){
  $(this).parent().remove();
  var field_id = $(this).data('id');
  filemanagerCallback(field_id);
})

function filemanagerCallback(field_id) {
  var url = '';
  $('#'+field_id).siblings('.image-preview').find('.gallery-item').each(function(i, e){
    if(i>0) {
      url += ',';
    }
    url += $(this).find('img').attr('src');
  })
  $('#'+field_id).val(url);
}