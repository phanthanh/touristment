tinymce.init({
  selector: '.editor',
  height: 200,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});
function tinymceReload() {
  tinymce.remove();
  tinymce.init({selector:'.editor'});
}
$('.collapse').collapse();


$('.tagsinput').each(function() {
  var tags = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: $(this).data('src')
  });
  tags.clearPrefetchCache();
  tags.initialize();

  $(this).tagsinput({
    itemValue: 'value',
    itemText: 'text',
    tagClass: 'badge badge-primary',
    typeaheadjs: {
      name: $(this).data('name'),
      displayKey: 'text',
      source: tags.ttAdapter()
    }
  });

})

$('.iframe-btn').fancybox({	
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
  'autoScale'    	: false
});

function openPopup(url)
{
  var w = 880;
  var h = 570;
  var l = Math.floor((screen.width - w) / 2);
  var t = Math.floor((screen.height - h) / 2);
  window.open(url, 'ResponsiveFilemanager', "scrollbars=1,width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
}
const LOCAL_URL = 'http://localhost/touristment/admin/';
function featuredArticle(article_id) {
  var td_featured = $('[data-article_id='+ article_id +']').find('[data-featured]'),
    featured = td_featured.data('featured'),
    update_featured = (featured ? 0 : 1);
  $.ajax({
    url: LOCAL_URL + 'ajax/article-update',
    type: 'POST',
    data: {
      article_id: article_id,
      data: {
        featured: update_featured
      }
    },
    success: function(res) {
      td_featured.attr('data-featured', update_featured);
      if(update_featured) {
        td_featured.removeClass().addClass('text-danger');
      }
      else {
        td_featured.removeClass().addClass('text-secondary');
      }
    }
  })
}

// Order
$(document).ready(function(){
  $('body').on('click', '.btn_order_status', function(e){
    if(confirm('Bạn có chắc?')) {
      var order_id = $(this).closest('tr').data('id');
      $.ajax({
        url: LOCAL_URL + 'ajax/order-status-update',
        type: 'POST',
        data: {
          order_id: order_id,
          status: $(this).data('status')
        },
        success:function(res) {
          location.reload();
        }
      })
    }
  })
  /* Save order note */ 
  $('body').on('change', '.save_note', function(e){
    loading($(this));
    $.ajax({
      url: LOCAL_URL + 'ajax/update-order-note',
      type: 'POST',
      data: {
        order_id: $(this).closest('tr').data('id'),
        order_note: $(this).val()
      }, 
      success: function(res) {
        destroyLoading($(this));
      }
    })
  })
})