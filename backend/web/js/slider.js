function responsive_filemanager_callback(field_id){
  var url = $('#'+field_id).val();
  var slide = '';
  var owlSlider = [];
  var multiple = url.match(/^\[(.+)\]$/);
  
  destroyExist(field_id);
  if(multiple) {
    var images = multiple[1].split(',');
    for (let i = 0; i < images.length; i++) {
      slide += genSliderItem(images[i]);
      owlSlider.push(genOwlSliderItem(images[i]));
    }
  }
  else {
    slide += genSliderItem(url);
    owlSlider.push(genOwlSliderItem(url));
  }
  genSliderItems(slide);
  genOwlSliderItems(owlSlider);
}

$('body').on('click', '.remove_slide', function(){
  $(this).closest('.slider_item').remove();
})

function genSliderItems(slide) {
  $('.slider-items').append(slide);
}
function genOwlSliderItems(owlSlider) {
  $('.slider-preview .owl-carousel').trigger('add.owl.carousel', owlSlider)
  .trigger('refresh.owl.carousel');
}
function destroyExist(field_id) {
  $('.slider_items').find('.slider_item[data-id='+ field_id +']').remove();
}


function genSliderItem(src) {
  let index = uniqid();
  let slide = '<div class="slider-item slider_item" data-id='+ index +'>';
  slide += '<img src='+ src +' />';
  slide += '<input type="hidden" name="slider[source][]" id="'+ index +'" value='+ src +' />';
  slide += '<a href="javascript:void(0)" class="edit-slide" onClick="openPopup(\'' + window.location.origin +'/filemanager/dialog.php?type=1&field_id='+ index +'&popup=1\')"><i class="material-icons">edit</i></a>';
  slide += '<a href="javascript:void(0)" class="remove-slide remove_slide"><i class="material-icons">delete_forever</i></a>';
  slide += '</div>';
  return slide;
}

function genOwlSliderItem(src) {
  let owlSlide = '<div class="item">';
  owlSlide += '<img src='+ src +' />';
  owlSlide += '</div>';
  return owlSlide;
}

function uniqid() {
  var S4 = function() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
  };
  return (S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4());
}