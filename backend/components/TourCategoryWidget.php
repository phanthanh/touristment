<?php 

namespace backend\components;

use yii\base\Widget;
use yii\helpers\Url;
use common\models\Category;

class TourCategoryWidget extends Widget
{

    const TYPE_TOUR = 'tour';
    
    public $cat_id;
    public $required = false;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $cats = (new Category)->getChildCatByParentId(0, self::TYPE_TOUR);

        $select = '<select name="cat_id" class="form-control" '. ($this->required ? 'required' : '') .'>';

        if($this->required) {
            $select .= '<option value="">Chọn danh mục</option>';
        }
        else {
            $select .= '<option value="0">Danh mục gốc</option>';
        }

        foreach ($cats as $cat ) {
            $prefix_count = substr_count($cat['path'], '-');
            $prefix = '';
            if($prefix_count > 1) {
                for($i=1; $i<$prefix_count; $i++) {
                    $prefix .= '-';
                }
            }
            $selected = $this->cat_id == $cat['id'] ? 'selected' : '';
            $select .= '<option value="'. $cat['id'] .'" '. $selected .'>'. $cat['title'] .'</option>';
        }
        $select .= '</select>';
        return $select;
    }
}