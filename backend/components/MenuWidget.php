<?php 

namespace backend\components;

use yii\base\Widget;
use common\models\Option;

class MenuWidget extends Widget
{
    public $name;

    public function init()
    {
        parent::init();
        if($this->name===null) {
            $this->name = 'main-menu';
        }
    }

    public function run()
    {
        $menu = Option::findOne(['name'=>$this->name]);
        $menuVal = json_decode($menu->value, true);
        $html = '<nav class="menu '. $this->name .'">';
        $html .= $this->generateMenu($menuVal[0]);
        $html .= '</nav>';

        return $html;
    }

    public function generateMenu($menu)
    {
        $html = '<ul>';
        foreach ($menu as $item) {
            $html .= '<li class="menu-item" data-title="'. $item['title'] .'" data-link="'. $item['link'] .'">';
            $html .= '<div class="menu-item-bar">';
            $html .= '<div class="menu-item-handle ui-sortable-handle">';
            $html .= '<span class="item-title"><span class="menu_item-title">'. $item['title'] .'</span></span>';
            $html .= '<span class="item-controls">';
            $html .= '<a class="menu_item-setting" href="javascript:void(0)"><i class="fa fa-caret-down"></i></a>';
            $html .= '</span>';
            $html .= '</div>';
            $html .= '<div class="menu_item-settings">';
            $html .= '<label>Tên menu</label>';
            $html .= '<input type="text" value="'. $item['title'] .'" class="form-control setting_title">';
            $html .= '<div class="menu-item-actions">';
            $html .= '<a href="javascript:void(0)" class="submit_delete">Xóa</a>';
            $html .= ' | ';
            $html .= '<a href="javascript:void(0)" class="submit_cancel">Hủy</a>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';

            if($item['children']) {
                $html .= $this->generateMenu($item['children'][0]);
            }

            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }
}


