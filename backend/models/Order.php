<?php
namespace backend\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Order extends ActiveRecord
{
    const STATUS_WAITING = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_CANCEL = 2;
    const STATUS_FINISHED = 3;

    public static function tableName()
    {
        return 'tbl_tour_order';
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_WAITING],
        ];
    }

    /*
      * Return query
    */ 
    public function getByFilter($search)
    {
        $query = self::find();
        if(isset($search['from_date']) && $search['from_date']) {
            $query->andWhere(['>', 'create_time', strtotime($search['from_date'])]);
        }
        if(isset($search['to_date']) && $search['to_date']) {
            $query->andWhere(['<', 'create_time', strtotime($search['to_date'])]);
        }
        if(isset($search['status']) && $search['status']>-1) {
            $query->andWhere('status=:status', [':status'=>$search['status']]);
        }
        if(isset($search['paid']) && $search['paid']>-1) {
            $query->andWhere('paid=:paid', [':paid'=>$search['paid']]);
        }
        if(isset($search['sort'])) {
            $query->orderBy($search['sort']);
        }
        return $query;
    }

    public function update($order_id, $data)
    {
        $order = self::findOne($order_id);
        if(isset($data['status']) && $data['status'] != $order->status) {
            $order->status = $data['status'];
        }
        if(isset($data['paid']) && $data['paid'] != $order->paid) {
            $order->paid = $data['paid'];
        }
        if(isset($data['custom_note']) && trim($data['custom_note']) != '') {
            $order->custom_note = trim($data['custom_note']);
        }
        if(isset($data['admin_note']) && trim($data['admin_note']) != '') {
            $order->admin_note = trim($data['admin_note']);
        }
        $order->update_by = Yii::$app->user->identity->id;
        $order->update_time = time();
        return $order->save();
    }
}