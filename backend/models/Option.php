<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class Option extends ActiveRecord
{
    
    public static function tableName()
    {
        return 'tbl_option';
    }

    public function getSlider()
    {
        $slider = self::findOne(['type'=>'slider']);
        if($slider) {
            return json_decode($slider->value, true);
        }
        return null;
    }

    
}
