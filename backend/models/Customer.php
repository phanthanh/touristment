<?php
namespace backend\models;

use yii\db\ActiveRecord;
use yii\db\Query;

class Customer extends ActiveRecord
{
    public static function tableName()
    {
        return 'customer';
    }
    
    /*
      * Return query
    */ 
    public function getByFilter($search)
    {
        $query = self::find();
        if(isset($search['keyword']) && trim($search['keyword'])!='') {
            $query->andWhere(['OR', 
                ['LIKE', 'fullname', trim($search['keyword'])],
                ['LIKE', 'email', trim($search['keyword'])],
                ['LIKE', 'phone', trim($search['keyword'])],
                ['LIKE', 'address', trim($search['keyword'])],
            ]);
        }
        
        return $query;
    }

}