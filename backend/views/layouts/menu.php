<?php  
use yii\helpers\Url;
?>
<aside>
    <div class="menu-wrap scrollbar">
        <ul>
            <li><a href="<?php echo Url::toRoute(['category/list-tour']) ?>"><i class="menu-icon material-icons">card_travel</i> <span>Danh mục Tour</span></a></li>
            <li><a href="<?php echo Url::toRoute('tour/index') ?>"><i class="menu-icon material-icons">airplanemode_active</i> <span>Danh sách Tour</span></a></li>
            <li><a href="<?php echo Url::toRoute('destination/index') ?>"><i class="menu-icon material-icons">terrain</i> <span>Điểm đến</span></a></li>
            <li><a href="<?php echo Url::toRoute('page/index') ?>"><i class="menu-icon material-icons">file_copy</i><span>Trang</span></a></li>
            <li><a href="<?php echo Url::toRoute(['category/list-article']) ?>"><i class="menu-icon material-icons">assignment</i> <span>Danh mục bài viết</span></a></li>
            <li><a href="<?php echo Url::toRoute('article/index') ?>"><i class="menu-icon material-icons">description</i> <span>Danh sách bài viết</span></a></li>
            <li><a href="<?php echo Url::toRoute('tag/index') ?>"><i class="menu-icon material-icons">local_offer</i> <span>Tag</span></a></li>
            <li><a href="<?php echo Url::toRoute('order/index') ?>"><i class="menu-icon material-icons">money</i> <span>Order</span></a></li>
            <li><a href="#"><i class="menu-icon material-icons">settings</i> <span>Cài đặt</span> <i class="toggle_child_menu material-icons">chevron_left</i></a>
                <ul class="collapsed">
                    <li><a href="<?php echo Url::toRoute('option/menu') ?>">Menu</a></li>
                    <li><a href="<?php echo Url::toRoute('option/slider') ?>">Slider</a></li>
                    <li><a href="<?php echo Url::toRoute('option/logo') ?>">Logo</a></li>
                </ul>
            </li>
            <li><a href=""><i class="menu-icon material-icons">perm_contact_calendar</i> <span>Khách hàng</span></a></li>
        </ul>
    </div>
</aside>