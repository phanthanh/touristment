<?php  
use yii\helpers\Url;
?>
<header>
    <div class="row">
        <div class="col-6">
            <a href="<?php echo Yii::$app->urlManagerFrontend->createUrl('site/index') ?>" target="_blank">Site</a>
        </div>
        <div class="col-6 d-flex justify-content-end">
            <div class="user-info">
                Xin chào <?php echo Yii::$app->user->identity->username ?>
            </div>
            <form action="<?php echo Url::toRoute('site/logout') ?>" method="POST">
                <button type="submit" class="btn-logout">Đăng xuất</button>
            </form>
        </div>
    </div>
</header>