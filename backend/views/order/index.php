<?php  
use yii\helpers\Url;
use common\models\Tour;
use common\models\Common;
?>

<table class="table box">
    <thead>
        <tr>
            <th>#</th>
            <th>Khách hàng</th>
            <th>Chuyến du lịch</th>
            <th></th>
            <th>Số lượng</th>
            <th>Tổng tiền</th>
            <th>Ngày đặt</th>
            <th>Yêu cầu</th>
            <th>Trạng thái</th>
            <th>Ghi chú</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $k => $item) : ?>

        <tr data-id="<?php echo $item['id'] ?>">
            <td></td>
            <td><?php echo $item['fullname'] ?><br/>
                <?php echo $item['phone'] ?><br/>
                <?php echo $item['email'] ?><br/>
            </td>
            <td><?php echo $item['title'] ?></a><br/>
                Khởi hành: <?php echo date('d/m/Y', strtotime($item['start_date'])) ?>
            </td>
            <td></td>
            <td><?php echo $item['slot_adult'] ?> Người lớn<br/>
                <?php echo $item['slot_kid'] ?> Trẻ em<br/>
                <?php echo $item['slot_baby'] ?> Em bé
            </td>
            <?php $price = json_decode($item['price'], true); ?>
            <td><?php echo Common::formatVnd(Tour::calcMoney($item['tour_id'], ($item['sale_price'] ?: $item['price']), $item['slot_adult'], $item['slot_kid'], $item['slot_baby'])) ?></td>
            <td><?php echo date('d/m/Y', $item['create_time']) ?></td>
            <td><?php echo $item['custom_note'] ?></td>
            <td class="d-flex flex-column">
                <?php switch ($item['status']) {
                    case '0':
                        echo '<span class="badge">Chưa xử lý</span>';
                        break;
                    case '1':
                        echo '<span class="badge">Đã xác thực</span>';
                        break;
                    case '2':
                        echo '<span class="badge">Đã thanh toán</span>';
                        break;
                    case '3':
                        echo '<span class="badge">Đã hủy</span>';
                        break;
                } ?>
                <?php switch ($item['status']) {
                    case '0':
                        ?>
                        <button class="btn btn-sm btn-success btn_order_status" data-status="1">Xác thực</button>
                        <button class="btn btn-sm btn-danger btn_order_status" data-status="3">Hủy chuyến</button>
                    <?php
                        break;
                    case '1': ?>
                        <button class="btn btn-sm btn-primary btn_order_status" data-status="2">Thanh toán</button>
                        <button class="btn btn-sm btn-danger btn_order_status" data-status="3">Hủy chuyến</button>
                    <?php 
                        break;
                } ?>
            </td>
            <td><textarea name="user_note" class="note save_note" placeholder="Nhập nội dung ghi chú ..."></textarea></td>
                
        </tr>

        <?php endforeach ?>
    </tbody>
</table>