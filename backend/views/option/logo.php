<?php  
use yii\web\View;  
use common\components\MyUtil;
?>
<div class="option-logo">
    <form action="" method="POST">
        <div class="form-group">
            <div class="image-preview">
                <div class="gallery-item">
                    <?php if(isset($logo['source'])) : ?>
                    <img src="<?php echo $logo['source'] ?>" alt="Logo">
                    <?php endif ?>
                </div>
            </div>
            <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=logo&popup=1')">Chọn logo</a>
            <input type="hidden" name="logo[source]" id="logo" value="<?php if(isset($logo['source'])) echo $logo['source'] ?>">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Lưu</button>
        </div>
    </form>
</div>
<?php 
$this->registerJsFile('@web/js/filemanager.js', ['depends'=>[yii\web\JqueryAsset::className()]]);