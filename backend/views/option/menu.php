<?php  
use yii\web\View;
use backend\components\MenuWidget;

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="menu-wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <div id="menu-settings-column">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" >Trang</button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show">
                            <div class="card-body">
                                <?php echo $this->render('./inc/menu_item', ['route'=>'page/index', 'type'=>'page', 'data'=>$pages]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" >Danh mục bài viết</button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse show">
                            <div class="card-body">
                                <?php echo $this->render('./inc/menu_item', ['route'=>'article/category', 'type'=>'article', 'data'=>$post_categories]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true">Danh mục Tour</button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse show">
                            <div class="card-body">
                                <?php echo $this->render('./inc/menu_item', ['route'=>'tour/category', 'type'=>'tour', 'data'=>$tour_categories]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <?php if(Yii::$app->session->hasFlash('form_message')) echo Yii::$app->session->getFlash('form_message') ?>

                <?php echo MenuWidget::widget(['name'=>$menu['name']]) ?>
                
                <div class="mb-3"></div>
                <form action="" method="POST">
                    <input type="hidden" name="menu_name" value="<?php echo $menu['name'] ?: 'main-menu'  ?>">
                    <input type="hidden" id="menu_value" name="menu_value" value='<?php echo $menu['value'] ?>'>
                    <button type="submit" class="btn btn-success">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
</div>