<?php  
use yii\web\View;
use common\components\MyUtil;
$slides = isset($slider['source']) ? $slider['source'] : array();

$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css');
$this->registerCssFile("https://fonts.googleapis.com/icon?family=Material+Icons");
$this->registerJsFile('@web/js/slider.js', ['depends'=>[\yii\web\JqueryAsset::className()]]);
?>
<div class="slider-wrap">
    <form action="" method="POST">
        <div class="row">
            <div class="col-sm-9">
                <div class="slider">
                    <div class="slider-preview box">
                        <div class="owl-carousel">
                        <?php foreach ($slides as $slide) : ?>
                            <div class="item">
                                <img src="<?php echo $slide ?>" />
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                    <div class="slider-settings box">
                        <div class="form-group">
                            <label for="">Thanh điều hướng</label>
                            <select name="slider[show_nav]" class="form-control w200">
                                <option value="1" <?php if(isset($slider['show_nav']) && $slider['show_nav']=='1') echo 'selected' ?>>Hiển thị</option>
                                <option value="0" <?php if(isset($slider['show_nav']) && $slider['show_nav']=='0') echo 'selected' ?>>Ẩn</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Nút điều hướng</label>
                            <select name="slider[show_dots]" class="form-control w200">
                                <option value="1" <?php if(isset($slider['show_dots']) && $slider['show_dots']=='1') echo 'selected' ?>>Hiển thị</option>
                                <option value="0" <?php if(isset($slider['show_dots']) && $slider['show_dots']=='0') echo 'selected' ?>>Ẩn</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Tự động chạy slide</label>
                            <select name="slider[auto_play]" class="form-control w200">
                                <option value="1" <?php if(isset($slider['auto_play']) && $slider['auto_play']=='1') echo 'selected' ?>>Có</option>
                                <option value="0" <?php if(isset($slider['auto_play']) && $slider['auto_play']=='0') echo 'selected' ?>>Không</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Thời gian delay</label>
                            <input type="number" name="slider[timeout]" value="<?php echo isset($slider['timeout']) && $slider['timeout'] ?: 5000 ?>" class="form-control w200" />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="slider-items slider_items">
                <?php if($slides) : ?>
                    <?php foreach ($slides as $k => $slide ) : ?>
                    <?php $index = md5(uniqid(rand(), true)); ?>
                        <div class="slider-item slider_item" data-id="<?php echo $index ?>">
                            <img src="<?php echo $slide ?>" />
                            <input type="hidden" name="slider[source][]" id="<?php echo $index ?>" value="<?php echo $slide ?>" data-id="<?php echo $k ?>" />
                            <a href="javascript:void(0)" class="edit-slide" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=<?php echo $index ?>&popup=1')"><i class="material-icons">edit</i></a>
                            <a href="javascript:void(0)" class="remove-slide remove_slide"><i class="material-icons">delete_forever</i></a>
                        </div>
                    <?php endforeach ?>
                    <?php endif ?>
                </div>
                <input type="hidden" id="new_slide">
                <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=new_slide&popup=1')">Thêm slide</a>
                <p class="text-secondary"><small>(Kích thước 1920x1080)</small></p>
            </div>
        </div>
    </form>
</div>
<?php 
$js = "$('.slider-preview .owl-carousel').owlCarousel({
    item: 1,
    margin: 0,
    dots: ". (isset($slider['show_dots']) && !$slider['show_dots'] ? 'false' : 'true' ) .",
    nav: ". (isset($slider['show_nav']) && !$slider['show_nav'] ? 'false' : 'true' ) .",
    autoPlay: ". (isset($slider['auto_play']) && !$slider['auto_play'] ? 'false' : 'true' ) .",
    responsive: {
        0: {
            items: 1
        }
    }
})";
$this->registerJs($js, View::POS_READY);