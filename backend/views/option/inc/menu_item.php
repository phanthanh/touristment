<?php 
use yii\helpers\Url;
?>
<div class="accordion-section-content " style="display: block;">
    <div class="inside">
        <div class="menu_items_wrap">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="<?php echo $type ?>-tab" data-toggle="tab" href="#<?php echo $type ?>" role="tab" aria-controls="home" aria-selected="true">Tất cả</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="<?php echo $type ?>_search-tab" data-toggle="tab" href="#<?php echo $type ?>_search" role="tab" aria-controls="profile" aria-selected="false">Tìm kiếm</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="<?php echo $type ?>" role="tabpanel" aria-labelledby="<?php echo $type ?>-tab">
                    <ul>
                    <?php foreach ($data as $k => $item) : ?>
                        <li>
                            <label class="menu_item">
                                <input type="checkbox" class="menu-item-checkbox" 
                                        data-title="<?php echo $item['title'] ?>" 
                                        data-link="<?php echo Yii::$app->urlManagerFrontend->createUrl([$route, 'id'=>$item['id'], 'alias'=>$item['alias']]) ?>" />
                                <?php echo $item['title'] ?>
                            </label>
                        </li>    
                    <?php endforeach ?>
                    </ul>
                </div>
                <div class="tab-pane fade" id="<?php echo $type ?>_search">
                    <p class="quick-search-wrap">
                        <label for="quick-search-taxonomy-category" class="screen-reader-text">Search</label>
                        <input type="search" class="quick-search" value="" name="quick-search-taxonomy-category" id="quick-search-taxonomy-category">
                        <span class="spinner"></span>
                        <input type="submit" name="submit" id="submit-quick-search-taxonomy-category" class="button button-small quick-search-submit hide-if-js" value="Search">
                    </p>

                    <ul id="category-search-checklist" data-wp-lists="list:category" class="categorychecklist form-no-clear"></ul>
                </div>
            </div>

            <div class="button-controls">
                <span class="list-controls">
                    <a href="/wp-admin/nav-menus.php?product_cat-tab=all&amp;selectall=1#taxonomy-product_cat" class="select-all aria-button-if-js" role="button">Select All</a>
                </span>
                <span class="add-to-menu">
                    <input type="submit" class="btn add_menu_item right" value="Add to Menu">
                    <span class="spinner"></span>
                </span>
            </div>
        </div>
    </div>
</div>