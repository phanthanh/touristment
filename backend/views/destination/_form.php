<?php
use common\components\MyUtil;
?>
<form action="" method="POST">
    <div class="row">
        <div class="col-sm-9">
            <?php if(Yii::$app->session->hasFlash('form_message')) : ?>
            <div class="form-group"><?php echo Yii::$app->session->getFlash('form_message'); ?></div>
            <?php endif ?>
            <div class="form-group">
                <label for="">Tên địa điểm du lịch</label>
                <input type="text" name="name" placeholder="Tên địa điểm" class="form-control" value="<?php if(isset($destination['name'])) echo $destination['name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="">Mô tả</label>
                <textarea name="description" class="editor"><?php if(isset($destination['description'])) echo $destination['description'] ?></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Cập nhật</button>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <div class="box">
                    <label for="">Chọn thể loại</label>
                    <select name="type" class="form-control">
                        <option value="vietnam" <?php if($destination['status']=='vietnam') echo 'selected' ?>>Trong nước</option>
                        <option value="world" <?php if($destination['status']=='world') echo 'selected' ?>>Quốc tế</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="box">
                    <label for="">Nổi bật <i class="fa fa-star"></i></label>
                    <select name="featured" class="form-control">
                        <option value="0" <?php if($destination['featured']=='0') echo 'selected' ?>>Không nổi bật</option>
                        <option value="1" <?php if($destination['featured']=='1') echo 'selected' ?>>Nổi bật</option>
                    </select>
                </div>
            </div>
            <?php if(isset($destination['status'])) : ?>
            <div class="form-group">
                <div class="box">
                    <label for="">Trạng thái</label>
                    <select name="status" class="form-control">
                        <option value="0" <?php if($destination['status']=='0') echo 'selected' ?>>Không hiển thị</option>
                        <option value="1" <?php if($destination['status']=='1') echo 'selected' ?>>Hiển thị</option>
                    </select>
                </div>
            </div>
            <?php endif ?>

            <div class="form-group">
                <div class="box">
                    <label for="">Ảnh đại diện</label>
                    <div class="image-preview">
                        <?php if($destination['image']) : ?>
                        <div class="gallery-item">
                            <span class="remove">&times;</span>
                            <img src="<?php echo $destination['image'] ?>" alt="">
                        </div>
                        <?php endif ?>
                    </div>
                    <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=destination_image&popup=1')">Ảnh đại diện</a>
                    <input id="destination_image" type="hidden" value="<?php if(isset($destination['image'])) echo $destination['image'] ?>" name="image">
                </div>
            </div>

        </div>
    </div>
</form>
<?php 
$this->registerJsFile('@web/js/filemanager.js', ['depends'=>[yii\web\JqueryAsset::className()]]);