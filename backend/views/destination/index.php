<?php  
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<form action="" method="GET">
    <div class="form-group">
        <input type="text" name="keyword" placeholder="Nhập tên địa điểm" value="<?php echo Yii::$app->request->get('keyword') ?>" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Tìm kiếm</button>
    <a href="<?php echo Url::toRoute('destination/create') ?>" class="btn btn-success">Thêm mới</a>

    <div class="mb-3"></div>
    <table class="table table-light">
        <thead>
            <tr>
                <td>#</td>
                <td>Ảnh đại diện</td>
                <td>Tên địa điểm</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($items as $item) : ?>
                <tr>
                    <td></td>
                    <td><img src="<?php echo $item['image'] ?>" alt="<?php echo $item['name'] ?>" class="img-thumb"></td>
                    <td><a href="<?php echo Url::toRoute(['destination/edit', 'id'=>$item['id']]) ?>"><?php echo $item['name'] ?></a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]); ?>  
</form>