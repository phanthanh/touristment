<?php if(Yii::$app->session->hasFlash('form_message')) echo Yii::$app->session->getFlash('form_message') ?>
<form action="" method="POST">
    <div class="form-group">
        <input type="text" name="name" class="form-control" value="<?php if(isset($tag)) echo $tag['name'] ?>"  placeholder="Tên tag" required>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success"><?php echo isset($tag) ? 'Cập nhật' : 'Thêm mới' ?></button>
    </div>
</form>