<div class="tag-wrap">
    <div class="card">
        <div class="card-head">
            <h5 class="card-title">Cập nhật tag: <strong><?php echo $tag['name'] ?></strong></h5>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', ['tag'=>$tag]) ?>
        </div>
    </div>
</div>