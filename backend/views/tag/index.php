<?php  
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-head">
                <h4 class="card-title">Thêm tag</h4>
            </div>
            <div class="card-body">
            <?php echo $this->render('_form') ?>                
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-head">
                <h4 class="card-title">Tìm kiếm</h4>
            </div>
            <div class="card-body">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col">
                            <input type="text" name="name" class="form-control" value="<?php echo Yii::$app->request->get('name') ?>" placeholder="Tên tag">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success">Tìm kiếm</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên</th>
                </tr>
            </thead>
            <tbody>
                
            <?php foreach ($items as $k => $item) : ?>
                
            <tr>
                <td></td>
                <td><a href="<?php echo Url::toRoute(['tag/edit', 'tag_id'=>$item['id']]) ?>"><?php echo $item['name'] ?></a></td>
            </tr>

            <?php endforeach ?>

            </tbody>
        </table>
    </div>
</div>