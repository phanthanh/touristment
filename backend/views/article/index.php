<?php  
use yii\helpers\Url;
?>
<div>
    <form action="" method="POST">
        <div class="form-group">
            <input type="text" name="keyword" class="form-control" value="<?php echo Yii::$app->request->get('keyword') ?>" placeholder="Tên bài viết, tên danh mục, ..">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Tìm kiếm</button>
            <a href="<?php echo Url::toRoute('article/new') ?>" class="btn btn-primary">Thêm mới bài viết</a>
        </div>
    </form>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Danh mục</th>
                <th scope="col">Tác giả</th>
                <th scope="col">Ngày tạo</th>
                <th scope="col">Nổi bật</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($items as $k => $item) : ?>
            <tr data-article_id="<?php echo $item['id'] ?>">
                <td></td>
                <td><a href="<?php echo Url::toRoute(['article/edit', 'article_id'=>$item['id']]) ?>"><?php echo $item['title'] ?></a></td>
                <td><?php echo $item['cat_title'] ?></td>
                <td><?php echo $item['autor'] ?></td>
                <td><?php echo date('d/m/Y', $item['create_time']) ?></td>
                <td><a href="javascript:void(0)" onClick="featuredArticle(<?php echo $item['id'] ?>)" data-featured="<?php echo $item['featured'] ?>" class="<?php echo $item['featured'] ? 'text-danger' : 'text-secondary' ?>"><i class="fa fa-star"></i></a></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>