<?php  
use yii\web\View;
use yii\helpers\Url;
use backend\components\ArticleCategoryWidget;
use common\components\MyUtil;
$session = Yii::$app->session;
?>
<?php if($session->hasFlash('form_message')) echo $session->getFlash('form_message') ?>
<form action="" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <input type="text" class="form-control" name="title" placeholder="Tiêu đề bài viết" value="<?php if(isset($article['title'])) echo $article['title'] ?>" required>
            </div>
            <div class="form-group">
                <textarea class="form-control editor" name="content" placeholder="Nội dung bài viết" cols="20"><?php if(isset($article['content'])) echo $article['content'] ?></textarea>
            </div>
            <div class="from-group">
                <button type="submit" class="btn btn-success"><?php echo isset($article['id']) ? 'Cập nhật' : 'Thêm mới' ?></button>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <div class="box">
                    <label for="">Danh mục bài viết</label>
                    <?php echo isset($article['cat_id']) ? ArticleCategoryWidget::widget(['cat_id'=>$article['cat_id']]) : ArticleCategoryWidget::widget() ?>
                    <div class="empty mb-3"></div>
                    <a href="<?php echo Url::toRoute('category/list-article') ?>" class="btn btn-warning" target="_blank"><i class="fa fa-plus"></i> Thêm mới danh mục</a>
                </div>
            </div>
            <div class="form-group">
                <div class="box">
                    <label for="">Tags</label>
                    <input type="text" name="article_tags" value="" class="form-control tagsinput article_tags" data-name="tag" data-src="<?php echo Url::to('@web/json/tags.json') ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="box">
                    <label for="">Ảnh đại diện</label>
                    <div class="image-preview">
                        <?php if(isset($article['image']) && $article['image']) : ?>
                        <div class="gallery-item"><img src="<?php echo $article['image'] ?>"><span class="remove" data-id="article_image">×</span></div>
                        <?php endif ?>
                    </div>
                    <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=article_image&popup=1')">Chọn ảnh đại diện</a>
                    <input id="article_image" type="hidden" value="<?php if(isset($article['image'])) echo $article['image'] ?>" name="image">
                </div>
            </div>
        </div>
    </div>
</form>
<?php 
$this->registerJsFile('@web/js/filemanager.js', ['depends'=>[yii\web\JqueryAsset::className()]]);

if(isset($tags) && $tags) {
    $js = '';
    foreach ($tags as $tag ) {
        $js .= '$(".article_tags").tagsinput("add", { "value": '. $tag['id'] .' , "text": "'. $tag['name'] .'"});';
    }
    $this->registerJs($js, View::POS_READY);
}