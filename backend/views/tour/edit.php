<?php  
$tour['itinerary'] = json_decode($tour['detail']['itinerary'], true);
$tour['images'] = json_decode($tour['images'], true);
$tour['price'] = json_decode($tour['price'], true);

echo $this->render('_form', ['tour'=>$tour]);