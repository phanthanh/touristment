<?php  
use yii\helpers\Url;
?>
<div>
    <a href="<?php echo Url::toRoute('tour/new') ?>" class="btn btn-success">Thêm mới</a>
</div>
<?php if($items) : ?>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Tiêu đề</th>
                <th>Giá</th>
                <th>Giảm giá</th>
                <th>Thời lượng</th>
                <th>Nổi bật</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $k => $item) : 
            $price = json_decode($item['price'], true); ?>

            <tr>
                <td></td>
                <td><a href="<?php echo Url::toRoute(['tour/edit', 'tour_id'=>$item['id']]) ?>"><?php echo $item['title'] ?></a></td>
                <td><?php echo $price['base'] ?></td>
                <td><?php echo $price['sale'] ?></td>
                <td><?php echo $item['duration_days'] ?> Ngày, <?php echo $item['duration_nights'] ?> Đêm</td>
                <td><?php echo $item['featured'] ? 'Có' : 'Không' ?></td>
            </tr>

        <?php endforeach ?>
        </tbody>
    </table>
<?php endif ?>