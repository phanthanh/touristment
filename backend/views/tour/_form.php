<?php  
use yii\helpers\Url;
?>
<form action="" method="POST">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-pills" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="main-tab" data-toggle="tab" href="#details" role="tab">Chi tiết</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab2" role="tab">Lịch trình</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab3" role="tab">Lịch khởi hành</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab4" role="tab">Thông tin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#tab5" role="tab">Thư viện ảnh</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="price-tab" data-toggle="tab" href="#tab6" role="tab">Bảng giá</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="review-tab" data-toggle="tab" href="#tab7" role="tab">Đánh giá</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="details" role="tabpanel">
                    <?php echo $this->render('./inc/details', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel">
                    <?php echo $this->render('./inc/itinerary', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab3" role="tabpanel">
                    <?php echo $this->render('./inc/departure', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab4" role="tabpanel">
                    <?php echo $this->render('./inc/information', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab5" role="tabpanel">
                    <?php echo $this->render('./inc/gallery', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab6" role="tabpanel">
                    <?php echo $this->render('./inc/price', ['tour'=>$tour]) ?>
                </div>
                <div class="tab-pane fade" id="tab7" role="tabpanel"></div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">Lưu</button>
        </div>
    </div>
</form>
<?php 
$this->registerJsFile(Url::to('@web/js/filemanager.js'), ['depends'=>[yii\web\JqueryAsset::className()]]);