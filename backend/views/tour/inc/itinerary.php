<?php 
use common\components\MyUtil;
?>
<div>
<div class="itinerary_block">

<?php $i = 0;
if(MyUtil::isExist($tour, 'itinerary')) : 
    foreach ($tour['itinerary'] as $k => $itinerary) : ?>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">
                    <span data-toggle="collapse" data-target="#tour-itinerary_<?php echo $k ?>" aria-expanded="true">
                        <span class="itinerary-label">Ngày <?php echo $k + 1 ?></span>,
                        <span class="itinerary-label">Kế hoạch ngày <?php echo $k + 1 ?></span>
                    </span>
                </h5>
            </div>
            <div id="tour-itinerary_<?php echo $k ?>" class="collapse show" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input type="text" name="itinerary[<?php echo $k ?>][title]" class="form-control" value="<?php echo $itinerary['title'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <div class="tour-itinerary">
                            <textarea name="itinerary[<?php echo $k ?>][desc]" class="editor"><?php echo $itinerary['desc'] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endforeach ?>
<?php else : ?>

    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">
                <span data-toggle="collapse" data-target="#tour-itinerary_0" aria-expanded="true" aria-controls="tour-itinerary_0">
                    <span class="itinerary-label">Ngày 1</span>,
                    <span class="itinerary-label">Kế hoạch ngày 1</span>
                </span>
            </h5>
        </div>
        <div id="tour-itinerary_0" class="collapse show" data-parent="#accordion">
            <div class="card-body">
                <div class="form-group">
                    <label>Tiêu đề</label>
                    <input type="text" name="itinerary[0][title]" class="form-control" value="Kế hoạch ngày 1">
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <div class="tour-itinerary">
                        <textarea name="itinerary[0][desc]" class="editor"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif ?>
</div>
<input type="hidden" id="itinerary-index" value="<?php echo $i ?>">
<div class="itinerary_button">
    <button type="button" onClick="addItinerary()" class="btn btn-warning">Thêm lịch trình</button>
</div>
</div>