<?php 
use common\components\MyUtil;
$include = MyUtil::isExist($tour, 'detail') ? MyUtil::isExist($tour['detail'], 'include') : '';
$exclude = MyUtil::isExist($tour, 'detail') ? MyUtil::isExist($tour['detail'], 'exclude') : '';
?>
<div class="form-group">
    <label for="">Giá bao gồm</label>
    <textarea name="include" class="form-control editor" cols="30" rows="10"><?php echo $include ?></textarea>
</div>
<div class="form-group">
    <label for="">Giá không bao gồm</label>
    <textarea name="exclude" class="form-control editor" cols="30" rows="10"><?php echo $exclude ?></textarea>
</div>