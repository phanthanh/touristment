<?php 
use common\components\MyUtil;
?>
<div>
<div class="departure_block">
<?php $i = 0;
if(MyUtil::isExist($tour, 'departure')) : 
    foreach ($tour['departure'] as $k => $departure) : 
        $timestamp = strtotime($departure['start_date']);
        $start_date = date('m/d/Y', $timestamp); ?>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">
                    <span data-toggle="collapse" data-target="#tour-departure_<?php echo $k ?>" aria-expanded="true">
                        Ngày <span class="departure-label"><?php echo $start_date ?></span>
                    </span>
                </h5>
            </div>
            <div id="tour-departure_<?php echo $k ?>" class="collapse show">
                <div class="card-body">

                    <?php if(isset($departure['id'])) : ?>
                    <input type="hidden" name="departure[<?php echo $k ?>][id]" value="<?php echo $departure['id'] ?>">
                    <?php endif ?>

                    <div class="form-group row">
                        <div class="col-auto">
                            <label>Ngày khởi hành</label>
                            <div class='input-group date'>
                                <input type='text' class="form-control datetimepicker" name="departure[<?php echo $k ?>][start_date]" value="<?php echo $start_date ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-auto">
                            <label>Số chỗ</label>
                            <div class="tour-departure">
                                <input type='number' name="departure[<?php echo $k ?>][slots]" class="form-control" value="<?php echo $departure['slots'] ?>" />
                            </div>
                        </div>
                        <div class="col-auto">
                            <label>Số chỗ đã đặt</label>
                            <div class="tour-departure">
                                <input type='number' name="departure[<?php echo $k ?>][booked_slots]" class="form-control" value="<?php echo $departure['booked_slots'] ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-auto">
                            <label>Giá</label>
                            <div class="tour-departure">
                                <input type='text' name="departure[<?php echo $k ?>][price]" class="form-control" value="<?php echo $departure['price'] ?>" />
                            </div>
                        </div>
                        <div class="col-auto">
                            <label>Giảm giá</label>
                            <div class="tour-departure">
                                <input type='text' name="departure[<?php echo $k ?>][sale_price]" class="form-control" value="<?php echo $departure['sale_price'] ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php $i++;
    endforeach ?>
<?php else : ?>
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">
                <span data-toggle="collapse" data-target="#tour-departure_0" aria-expanded="true">
                    Ngày <span class="departure-label"><?php echo date('m/d/Y') ?></span>
                </span>
            </h5>
        </div>
        <div id="tour-departure_0" class="collapse show">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-auto">
                        <label>Ngày khởi hành</label>
                        <div class='input-group date'>
                            <input type='text' class="form-control datetimepicker" name="departure[0][start_date]" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-auto">
                        <label>Số chỗ</label>
                        <div class="tour-departure">
                            <input type='number' name="departure[0][slots]" class="form-control" />
                        </div>
                    </div>
                    <div class="col-auto">
                        <label>Số chỗ còn lại</label>
                        <div class="tour-departure">
                            <input type='number' name="departure[0][booked_slots]" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-auto">
                        <label>Giá</label>
                        <div class="tour-departure">
                            <input type='text' name="departure[0][price]" class="form-control" />
                        </div>
                    </div>
                    <div class="col-auto">
                        <label>Giảm giá</label>
                        <div class="tour-departure">
                            <input type='text' name="departure[0][sale_price]" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

</div>
<input type="hidden" id="departure-index" value="<?php echo $i ?>">
<div class="departure_button">
    <button type="button" onClick="addDeparture()" class="btn btn-warning">Thêm ngày khởi hành</button>
</div>
</div>