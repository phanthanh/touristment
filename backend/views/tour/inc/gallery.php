<?php 
use common\components\MyUtil;
$gallery = MyUtil::isExist($tour, 'detail') ? MyUtil::isExist($tour['detail'], 'gallery') : '';
?>
<div class="form-group">
    <div class="image-preview">
        <?php if($gallery) : 
            $galleryArr = explode(',', $gallery);
            foreach($galleryArr as $image) : ?>

        <div class="gallery-item">
            <img src="<?php echo $image ?>">
            <span class="remove" data-id="tour_detail_gallery">&times;</span>
        </div>

        <?php endforeach ?>
        <?php endif ?>
    </div>
    <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=tour_detail_gallery&popup=1')">Chọn ảnh</a>
    <input id="tour_detail_gallery" type="hidden" value="<?php echo $gallery ?>" name="gallery" data-multiple="true">
    <p><small>(Kích thước phù hợp 750x450)</small></p>
</div>