<?php 
use yii\helpers\Url;
use backend\components\TourCategoryWidget;
use yii\web\View;
use common\components\MyUtil;
$priceArr = MyUtil::isExist($tour, 'price', []);
?>
<div class="row">
    <div class="col-sm-9">
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Tiêu đề</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="title" value="<?php echo MyUtil::isExist($tour, 'title') ?>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Giới thiệu</label>
            <div class="col-sm-10">
                <textarea class="form-control editor" name="intro_text"><?php echo MyUtil::isExist($tour, 'intro_text') ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Địa điểm</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-2">
                        <select name="departure_location" class="form-control">
                            <option value="">Điểm khởi hành</option>
                            <option value="hanoi" <?php if(MyUtil::isExist($tour, 'departure_location') == 'hanoi') echo 'selected' ?>>Hà Nội</option>
                            <option value="danang" <?php if(MyUtil::isExist($tour, 'departure_location') == 'danang') echo 'selected' ?>>Đà Nẵng</option>
                            <option value="hochiminh" <?php if(MyUtil::isExist($tour, 'departure_location') == 'hochiminh') echo 'selected' ?>>Hồ Chí Minh</option>
                        </select>
                    </div>
                    <div class="col">
                        <input type="text" name="destination" data-name="destination" class="form-control tagsinput destination_tag" placeholder="Điểm đến" data-src="<?php echo Url::to('@web/json/destinations.json') ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Giá</label>
            <div class="col-auto">
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control" name="price[base]" value="<?php echo MyUtil::isExist($priceArr, 'price') ?>" placeholder="Giá gốc"/>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="price[sale]" value="<?php echo MyUtil::isExist($priceArr, 'sale') ?>" placeholder="Giá sale" />
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="price[kid]" value="<?php echo MyUtil::isExist($priceArr, 'kid') ?> >" placeholder="Giá trẻ em" />
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="price[baby]" value="<?php echo MyUtil::isExist($priceArr, 'baby') ?>" placeholder="Giá em bé" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Thời lượng</label>
            <div class="col-auto">
                <div class="row">
                    <div class="col">
                        <input type="number" class="form-control" name="duration_days" value="<?php echo MyUtil::isExist($tour, 'duration_days') ?>" placeholder="X Ngày"> 
                    </div>
                    <div class="col">
                        <input type="number" class="form-control" name="duration_nights" value="<?php echo MyUtil::isExist($tour, 'duration_nights') ?>" placeholder="X Đêm">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">Ảnh Full</label>
            <div class="col-sm-10">
                <div class="image-preview full">
                    <?php if(isset($tour['images']['full']) && $tour['images']['full']) : ?>
                    <div class="gallery-item">
                        <span class="remove">&times;</span>
                        <img src="<?php echo $tour['images']['full'] ?>" />
                    </div>
                    <?php endif ?>
                </div>
                <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=tour_image_full&popup=1')">Chọn ảnh</a>
                <p><small>(Dùng làm ảnh nền cho trang chi tiết 1920x375)</small></p>
                <input id="tour_image_full" type="hidden" value="<?php if(isset($tour['images']['full'])) echo $tour['images']['full'] ?>" name="images[full]">
            </div>
        </div>
        
    </div>
    <div class="col-sm-3">
        <div class="form-group box">
            <label for="" class="">Danh mục</label>
            <div class="mb-3">
                <?php $params = ['required'=>true]; 
                    if(isset($tour['cat_id'])) {
                        $params['cat_id'] = $tour['cat_id'];
                    }
                    echo TourCategoryWidget::widget($params) ?>
        
            </div>
            <a href="<?php echo Url::toRoute(['category/new', 'type'=>'tour']) ?>" target="_blank" class="btn btn-warning"><i class="fa fa-plus"></i> Thêm mới danh mục</a>
        </div>

        <div class="form-group">
            <div class="box">
                <label for="" class="">Ảnh đại diện</label>
                <div class="">
                    <div class="image-preview">
                        <?php if(isset($tour['images']['intro']) && $tour['images']['intro']) : ?>
                        <div class="gallery-item">
                            <span class="remove">&times;</span>
                            <img src="<?php echo $tour['images']['intro'] ?>" />
                        </div>
                        <?php endif ?>
                    </div>
                    <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=tour_image_intro&popup=1')">Chọn ảnh</a>
                    <input id="tour_image_intro" type="hidden" value="<?php if(isset($tour['images']['intro'])) echo $tour['images']['intro'] ?>" name="images[intro]">
                </div>
            </div>
        </div>

        <div class="form-group box">
            <label for="">Nổi bật <i class="fa fa-star"></i></label>
            <select name="featured" class="form-control">
                <option value="0" <?php if(MyUtil::isExist($tour, 'featured') == '0') echo 'selected' ?>>Không nổi bật</option>
                <option value="1" <?php if(MyUtil::isExist($tour, 'featured') == '1') echo 'selected' ?>>Nổi bật</option>
            </select>
        </div>

        <div class="form-group box">
            <label for="">Hiển thị</label>
            <select name="status" class="form-control">
                <option value="1" <?php if(MyUtil::isExist($tour, 'status') == '1') echo 'selected' ?>>Hiển thị</option>
                <option value="0" <?php if(MyUtil::isExist($tour, 'status') == '0') echo 'selected' ?>>Không hiển thị</option>
            </select>
        </div>
    </div>
</div>
<?php if(MyUtil::isExist($tour, 'destinations')) :
$js = '';
foreach ($tour['destinations'] as $destination ) {
    $js .= '$(".destination_tag").tagsinput("add", { "value": '. $destination['id'] .' , "text": "'. $destination['name'] .'"});';
}
if($js) {
    $this->registerJs($js, View::POS_READY);
}
endif;