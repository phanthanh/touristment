<?php  
use yii\helpers\Url;
use backend\components\TourCategoryWidget;
use common\components\MyUtil;
?>
<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-head">
                <h4 class="card-title">Thêm mới danh mục Tour</h4>
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    <input type="hidden" name="type" class="form-control" value="tour">
                    <div class="form-group">
                        <label for="">Tên danh mục</label>
                        <input type="text" name="title" class="form-control" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="">Danh mục cha</label>
                        <?php echo TourCategoryWidget::widget() ?>
                    </div>
                    <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=tour_detail_images&popup=1')">Chọn ảnh đại diện</a>
                        <input id="tour_detail_images" type="hidden" value="" name="tour_detail_images">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
    <?php if(Yii::$app->session->hasFlash('form_message')) echo Yii::$app->session->getFlash('form_message') ?>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Tên danh mục</th>
            </tr>
        </thead>
        <tbody>
            
        <?php foreach ($items as $k => $item) : 
            $level = substr_count($item['path'], '-') - 1; ?>
            
        <tr>
            <td></td>
            <td class="level-<?php echo $level ?>"><a href="<?php echo Url::toRoute(['category/edit', 'cat_id'=>$item['id']]) ?>"><?php for($i=0;$i<$level;$i++) {echo '&nbsp;&nbsp;&nbsp;';} ?><?php echo str_pad('', $level, '-') ?> <?php echo $item['title'] ?></a></td>
        </tr>

        <?php endforeach ?>

        </tbody>
    </table>
    </div>
</div>