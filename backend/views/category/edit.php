<?php  
use yii\web\View;
use backend\components\TourCategoryWidget;
use common\components\MyUtil;
?>
<?php if(Yii::$app->session->hasFlash('form_message')) echo Yii::$app->session->getFlash('form_message') ?>
<div class="card">
    <div class="card-head">
        <h3 class="card-title">Cập nhật: <?php echo $cat['title'] ?></h3>
    </div>
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <label for="">Tiêu đề</label>
                <input type="text" class="form-control" name="title" placeholder="Tên danh mục" value="<?php echo $cat['title'] ?>" required>
            </div>
            <div class="form-group">
                <label for="">Danh mục cha</label>
                <?php echo TourCategoryWidget::widget(['cat_id'=>$cat['parent_id']]) ?>
            </div>
            <div class="form-group">
                <label for="">Mô tả</label>
                <textarea class="form-control editor" name="" placeholder="Mô tả"><?php echo $cat['description'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="">Ảnh đại diện</label>
                <div class="image-preview">
                    <?php if($cat['image']) : ?>
                        <div class="gallery-item">
                            <img src="<?php echo $cat['image'] ?>" alt="">
                            <span class="remove">&times;</span>
                        </div>
                    <?php endif ?>
                </div>
                <input type="hidden" name="image" id="cat_image" value="<?php echo $cat['image'] ?>">
                <a href="javascript:void(0)" class="btn btn-secondary" onClick="openPopup('<?php echo MyUtil::baseUrl() ?>/filemanager/dialog.php?type=1&field_id=cat_image&popup=1')">Chọn ảnh</a>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-auto">
                        <label for="">Danh mục nổi bật</label>
                        <select name="featured" class="form-control">
                            <option value="0" <?php if($cat['featured']==0) echo 'selected' ?>>Không</option>
                            <option value="1" <?php if($cat['featured']==1) echo 'selected' ?>>Có</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <label for="">Trạng thái</label>
                        <select name="status" class="form-control">
                            <option value="0" <?php if($cat['status']==0) echo 'selected' ?>>Không hiển thị</option>
                            <option value="1" <?php if($cat['status']==1) echo 'selected' ?>>Hiển thị</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Lưu</button>
            </div>
        </form>
    </div>
</div>
<?php 
$this->registerJsFile('@web/js/filemanager.js', ['depends'=>[yii\web\JqueryAsset::className()]]);