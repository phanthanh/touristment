<?php 
use yii\helpers\Url;
use yii\web\View;
use backend\components\TourCategoryWidget;

$session = Yii::$app->session;
?>
<?php if($session->hasFlash('form_message')) echo $session->getFlash('form_message') ?>
<form action="" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Tiêu đề danh mục" value="<?php echo Yii::$app->request->post('title') ?>" required>
            </div>
            <div class="form-group">
                <textarea class="form-control editor" name="description" placeholder="Mô tả"><?php echo Yii::$app->request->post('description') ?></textarea>
            </div>
            <input type="hidden" name="type" value="<?php echo Yii::$app->request->get('type', 'tour') ?>">
            <div class="form-group">
                <button type="submit" class="btn btn-success" onClick="submit()">Lưu</button>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <div class="box">
                    <label for="">Thuộc danh mục</label>
                    <?php echo TourCategoryWidget::widget(['type'=>Yii::$app->request->get('type', 'tour')]) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="box">
                    <label for="">Thẻ tags</label>
                    <input type="text" name="tags" value="<?php echo Yii::$app->request->post('tags') ?>" class="form-control">
                    <p class="note">Ngăn cách bằng các dấu phẩy</p>
                </div>
            </div>
            <div class="form-group">
                <div class="box">
                    <label for="">Ảnh đại diện</label>
                    <input type="file">
                </div>
            </div>
        </div>
    </div>
</form>
<?php 
$this->registerJsFile('@backend/web/js/filemanager.js', ['depends'=>[yii\web\JqueryAsset::className()]]);