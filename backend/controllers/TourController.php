<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Tour;
use common\models\TourDetail;
use common\models\TourDeparture;
use common\models\Destination;

/**
 * Site controller
 */
class TourController extends Controller
{

    public function actionIndex()
    {   
        $search = Yii::$app->request->get();
        $query = (new Tour)->getByFilter($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
			  $items = $query->offset($pages->offset)
			        ->limit($pages->limit)
                    ->all();

        return $this->render('index', array (
            'items'=>$items,
            'pages'=>$pages
        ) );
    }

    public function actionEdit()
    {   
        $tour_id = Yii::$app->request->get('tour_id');
        $tour = Tour::findOne($tour_id);

        if(!$tour) {
            return $this->redirect(Url::toRoute('tour/index'));
        }

        $tour = $tour->attributes;
        $tour['detail'] = TourDetail::findOne(['tour_id'=>$tour_id]);
        $tour['departure'] = TourDeparture::findAll(['tour_id'=>$tour_id]);
        $tour['destinations'] = (new Destination)->getDestinations( explode(',', $tour['destination']) );
        
        $data = Yii::$app->request->post();
        if($data) {
            // var_dump($data);die;
            $session = Yii::$app->session;
            if((new Tour())->edit($tour['id'], $data)) {
                $session->setFlash('form_error', 'Có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại sau!');
            }
            else {
                $session->setFlash('form_error', 'Cập nhật thành công');
            }
            return $this->redirect(Url::current());
        }

        return $this->render('edit', array (
            'tour'=>$tour
        ) );
    }

    public function actionNew()
    {   
        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $session = Yii::$app->session;
            
            $tour_id = (new Tour())->create($data);
            if($tour_id) {
                $session->setFlash('form_error', '<div class="alert alert-danger">Có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại sau!</div>');
                return $this->redirect(Url::toRoute(['tour/edit', 'tour_id'=>$tour_id]));
            }
            else {
                $session->setFlash('form_error', '<div class="alert alert-success">Thêm mới thành công</div>');
            }
        }
        return $this->render('new');
    }
}