<?php 

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Article;
use common\models\Order;
use common\models\TourDeparture;

class AjaxController extends Controller
{
    
    public function actionArticleUpdate()
    {
        $article_id = Yii::$app->request->post('article_id');
        $data = Yii::$app->request->post('data');
        $article = Article::findOne($article_id);
        if($article) {
            return (new Article)->edit($article_id, $data);
        }
        return false;
    }

    public function actionOrderStatusUpdate()
    {
        $order_id = Yii::$app->request->post('order_id');
        $status = Yii::$app->request->post('status');

        $order = Order::findOne($order_id);
        if(!$order) return;
        $order->status = $status;
        $order->update_by = Yii::$app->user->id;
        $order->update_time = time();
        $order->save();

        $tour_departure = TourDeparture::findOne($order['departure_id']);
        if(!$tour_departure) return;
        if($status==1) {
            $slots = $order['slot_adult'] + $order['slot_kid'];
            $tour_departure->booked_slots += $slots;
        }
        if($status==3) {
            $slots = $order['slot_adult'] + $order['slot_kid'];
            $tour_departure->booked_slots -= $slots;
        }
        return $tour_departure->save();
    }
}
