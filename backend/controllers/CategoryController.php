<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Category;
use common\models\Article;
use yii\helpers\Url;

/**
 * Site controller
 */
class CategoryController extends Controller
{
    const TYPE_TOUR = 'tour';
    const TYPE_ARTICLE = 'article';

    public function actionListTour()
    {   
        $cat_type = Yii::$app->request->get('type', self::TYPE_TOUR);
        if(Yii::$app->request->isPost) {
            if((new Category)->edit(Yii::$app->request->post())) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Thêm mới thành công</div>');
            }
            else {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại sau!</div>');
            }
        }
        $items = (new Category)->getChildCatByParentId(0, $cat_type);
        return $this->render('list_tour', [
            'items'=>$items
        ]);
    }

    public function actionListArticle()
    {   
        $cat_type = Yii::$app->request->get('type', self::TYPE_ARTICLE);
        if(Yii::$app->request->isPost) {
            if((new Category)->edit(Yii::$app->request->post())) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Thêm mới thành công</div>');
            }
            else {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại sau!</div>');
            }
        }
        $items = (new Category)->getChildCatByParentId(0, $cat_type);
        return $this->render('list_article', [
            'items'=>$items
        ]);
    }

    public function actionEdit()
    {   
        $cat_id = Yii::$app->request->get('cat_id');
        $cat = Category::findOne($cat_id);

        if(!$cat) {
            return $this->redirect(Url::toRoute('category/index'));
        }

        $cat = $cat->attributes;
        $parent_cat = (new Category)->getParentCatByPath($cat['id'], $cat['path']);
        $cat['parent_id'] = $parent_cat ? $parent_cat->id : 0;

        if(Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            if((new Category())->edit(Yii::$app->request->post(), $cat['id'])) {
                $session->setFlash('form_message', '<div class="alert alert-success">Cập nhật thành công</div>');
            }
            else {
                $session->setFlash('form_message', '<div class="alert alert-danger">Có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại sau!</div>');
            }
            return $this->redirect(Url::current());
        }

        return $this->render('edit', array (
            'cat'=>$cat
        ) );
    }

}