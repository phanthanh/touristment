<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Tag;

/**
 * Site controller
 */
class TagController extends Controller
{
    public function actionIndex()
    {
        if(Yii::$app->request->isPost) {
            $tag = Yii::$app->request->post('name');
            if(Tag::findOne(['name'=>$tag])) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Tag đã tồn tại</div>');
            }
            else {
                $tagModel = new Tag;
                $tagModel->name = $tag;
                if($tagModel->save()) {
                    $tagModel->createTagJsonFile();
                    Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Thêm mới tag <strong></strong> thành công</div>');
                }
            }
        }
        $search = Yii::$app->request->get();
        $query = (new Tag)->getByFilter($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
			  $items = $query->offset($pages->offset)
			        ->limit($pages->limit)
                    ->all();

        return $this->render('index', array (
            'items'=>$items,
            'pages'=>$pages
        ) );
    }

    public function actionEdit()
    {
        $tagId = Yii::$app->request->get('tag_id');
        $tag = Tag::findOne($tagId);
        if(!$tag) {
            return $this->redirect(Url::toRoute('tag/index'));
        }
        if(Yii::$app->request->isPost) {
            $tagName = Yii::$app->request->post('name');
            $tag->name = $tagName;
            if($tag->save()) {
                $tagModel->createTagJsonFile();
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Cập nhật <strong></strong> thành công</div>');
            }
        }
        return $this->render('edit', array (
            'tag'=>$tag,
        ) );
    }
}