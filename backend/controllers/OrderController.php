<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Order;

/**
 * Site controller
 */
class OrderController extends Controller
{
    public function actionIndex()
    {
        $search = Yii::$app->request->get();
        $query = (new Order)->getByFilter($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('index', [
            'items'=>$items
        ]);
    }
}