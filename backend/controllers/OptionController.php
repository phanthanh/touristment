<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use common\models\Option;
use common\models\Category;
use common\models\Page;

/**
 * Site controller
 */
class OptionController extends Controller
{
    public function actionMenu()
    {
        $menu_name = Yii::$app->request->get('menu_name', 'main-menu');
        if(Yii::$app->request->isPost) {
            if((new Option)->configMenu(Yii::$app->request->post())) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Thêm mới thành công</div>');
            }
            else {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Có lỗi xảy ra. Vui lòng thử lại sau!</div>');
            }
        }
        $menu = Option::findOne(['name'=>$menu_name]);
        $pages = Page::find()->where(['status'=>'publish'])->all();
        $post_categories = Category::find()->where(['type'=>'post'])->orderBy('path')->all();
        $tour_categories = Category::find()->where(['type'=>'tour'])->orderBy(['path'=>'ASC'])->all();

        return $this->render('menu', [
                'menu'=>$menu,
                'pages'=>$pages,
                'post_categories'=>$post_categories,
                'tour_categories'=>$tour_categories,
            ]);
    }

    public function actionSlider()
    {   
        if(Yii::$app->request->isPost) {
            $slider = Yii::$app->request->post('slider');
            (new Option)->updateSlider($slider);
        }
        $slider = (new Option)->getSlider();
        return $this->render('slider', [
            'slider'=>$slider
        ]);
    }

    public function actionLogo()
    {
        if(Yii::$app->request->isPost) {
            $logo = Yii::$app->request->post('logo');
            (new Option)->updateLogo($logo);
        }
        $logo = (new Option)->getLogo();
        return $this->render('logo', [
            'logo'=>$logo
        ]);
    }

    public function actionPreset()
    {
        $less = new lessc;
        // create a new cache object, and compile
        $cache = $less->cachedCompile($inputFile);
        file_put_contents($outputFile, $cache["compiled"]);

        $less->setVariables(array(
            "color" => "red",
            "base" => "960px"
        ));

        // the next time we run, write only if it has updated
        $last_updated = $cache["updated"];
        $cache = $less->cachedCompile($cache);
        if ($cache["updated"] > $last_updated) {
            file_put_contents($outputFile, $cache["compiled"]);
        }
        
        $presets = (new Option)->getPresets();
        return $this->render('slide', [
            'presets'=>$presets
        ]);
    }
    
}