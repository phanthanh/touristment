<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Category;
use common\models\Article;
use common\models\Tag;

/**
 * Site controller
 */
class ArticleController extends Controller
{

    public function actionIndex()
    {   
        $search = Yii::$app->request->get();
        $query = (new Article)->getByFilters($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
                
        return $this->render('index', array (
            'items'=>$items,
            'pages'=>$pages
        ) );
    }

    public function actionEdit()
    {   
        $articleId = Yii::$app->request->get('article_id');
        $article = Article::findOne($articleId);
        
        if(!$article) {
            return $this->redirect(Url::toRoute('article/index'));
        }

        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $session = Yii::$app->session;
            if((new Article())->edit($articleId, $data)) {
                $session->setFlash('form_message', 'Có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại sau!');
            }
            else {
                $session->setFlash('form_message', 'Cập nhật thành công');
            }
            return $this->redirect(Url::current());
        }

        $tags = (new Tag)->getTagsByObject($articleId, Article::tableName());
        return $this->render('edit', array (
            'article'=>$article,
            'tags'=>$tags
        ) );
    }

    public function actionNew()
    {   
        $data = Yii::$app->request->post();
        if($data) {
            $session = Yii::$app->session;
            $article_id = (new Article())->edit(false, $data);
            if($article_id) {
                $session->setFlash('form_message', '<div class="alert alert-danger">Có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại sau!</div>');
                return $this->redirect(Url::toRoute(['article/edit', 'article_id'=>$article_id]));
            }
            else {
                $session->setFlash('form_message', '<div class="alert alert-success">Thêm mới thành công</div>');
            }
        }
        return $this->render('new');
    }

}