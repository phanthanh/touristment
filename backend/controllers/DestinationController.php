<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Destination;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\Common;

/**
 * Site controller
 */
class DestinationController extends Controller
{

    public function actionIndex()
    {
        $search = Yii::$app->request->get();
        $query = (new Destination)->getByFilters($search);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('index', [
            'items' => $items,
            'pages' => $pages
        ]);
    }

    public function actionEdit()
    {
        $destination_id = Yii::$app->request->get('id');
        $destination = Destination::findOne($destination_id);

        if(Yii::$app->request->isPost) {
            $destination->name = Yii::$app->request->post('name');
            $destination->alias = Common::convertToAlias($destination->name);
            $destination->image = Yii::$app->request->post('image');
            $destination->status = Yii::$app->request->post('status');
            $destination->featured = Yii::$app->request->post('featured');
            if($destination->save()) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Cập nhật thành công</div>');
            }
            else {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Cập nhật thất bại</div>');
            }
        }

        return $this->render('edit', [
            'destination'=>$destination
        ]);
    }

    public function actionCreate()
    {
        $model = new Destination();
        if (Yii::$app->request->isPost) {
            $existDestination = $this->existDestinationByName(Yii::$app->request->post('name'));
            if($existDestination) {
                Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Đã tồn tại địa điểm này</div>');
            }
            else {
                $model->name = Yii::$app->request->post('name');
                $model->alias = Common::convertToAlias($model->name);
                $model->type = Yii::$app->request->post('type', 'vietnam');
                $model->image = Yii::$app->request->post('image');
                if($model->save()) {
                    Yii::$app->session->setFlash('form_message', '<div class="alert alert-success">Thêm mới thành công</div>');
                    $this->createDestinationJsonFile();
                    return $this->redirect(Url::toRoute(['destination/edit', 'id'=>$model->id]));
                }
                else {
                    Yii::$app->session->setFlash('form_message', '<div class="alert alert-danger">Thêm mới thất bại</div>');
                }
            }
        }
        return $this->render('new');
    }

    public function existDestinationByName($destination_name)
    {
        $destination = Destination::findOne(['name'=>trim($destination_name)]);
        return $destination ? true : false;
    }

    public function createDestinationJsonFile()
    {
        $destinations = Destination::findAll(['status'=>1]);
        $data = array();
        foreach ($destinations as $destination ) {
            array_push($data, ['value'=>$destination['id'], 'text'=>$destination['name']]);
        }
        $fp = fopen('json/destinations.json', 'w');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }
}